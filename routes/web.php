<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'auth:sanctum', config('jetstream.auth_session')], function () {
    Route::group(['middleware' => 'active'], function () {
        Route::get('/', \App\Http\Controllers\MainController::class)->name('index');

        Route::prefix('favorites')
            ->controller(\App\Http\Controllers\FavoritesController::class)
            ->group(function () {
                Route::post('/add', 'store')->name('favorites.add');
                Route::delete('/delete/{id}', 'delete')->name('favorites.delete');
            });

        Route::get('/read_notifications', \App\Http\Controllers\ReadNotificationController::class)->name('read-notifications');

        Route::prefix('tasks')
            ->controller(\App\Http\Controllers\TaskController::class)
            ->group(function () {
                Route::get('/', 'index')->name('tasks.index');
                Route::put('/{task}', 'update')->name('tasks.update');
            });

        Route::prefix('results')
            ->controller(\App\Http\Controllers\ResultController::class)
            ->group(function () {
                Route::get('/', 'index')->name('results.index');
                Route::get('/export/{period}', 'export')->name('results.export');
            });

        Route::prefix('plannings')
            ->controller(\App\Http\Controllers\PlanningController::class)
            ->group(function () {
                Route::get('/lists', 'list')->name('planning.list');
                Route::get('/', 'index')->name('planning.index');
                Route::post('/', 'store')->name('planning.task.store');
                Route::put('/{task}', 'update')->name('planning.task.update');
                Route::delete('/{task}', 'deleteTask')->name('planning.task.delete');
            });

        Route::prefix('reporting_periods')
            ->controller(\App\Http\Controllers\PeriodController::class)
            ->group(function () {
                Route::post('/', 'store')->name('periods.store');
                Route::post('/{reporting_period}', 'working')->name('periods.working');
                Route::post('/{user_reporting_period}/approve', 'approve')->name('periods.approve');
                Route::post('/{reporting_period}/planning', 'planning')->name('periods.planning');
                Route::post('/{reporting_period}/approved', 'approved')->name('periods.approved');
                Route::post('/{reporting_period}/to_payment', 'toPayment')->name('periods.toPayment');
            });

        Route::group(['prefix' => 'administrations'], function () {
            Route::get('/', \App\Http\Controllers\Administration\AdministrationController::class)
                ->name('administration.index');

            Route::resource(
                'users',
                \App\Http\Controllers\Administration\AdministrationUserController::class,
                ['as' => 'administration']
            );

            Route::prefix('user_tree')
                ->controller(\App\Http\Controllers\Administration\AdministrationUserTreeController::class)
                ->as('administration.')
                ->group(function () {
                    Route::get('/', 'index')->name('users.tree.index');
                    Route::post('/change', 'change')->name('users.tree.change');
                });
        });

        Route::prefix('uploads')
            ->controller(\App\Http\Controllers\FileController::class)
            ->group(function () {
                Route::post('/image', 'uploadImage')->name('uploads.image');
            });

        Route::prefix('easter_eggs')
            ->controller(\App\Http\Controllers\EasterEggsController::class)
            ->group(function () {
                Route::get('/snake', 'snake')->name('easter_eggs.snake');
                Route::post('/snake', 'score')->name('easter_eggs.snake.score');
            });

        Route::any('test', \App\Http\Controllers\TestController::class)->name('test');
    });
});
