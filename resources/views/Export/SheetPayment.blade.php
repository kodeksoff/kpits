<table>
    <thead>
    <tr>
        <th>Имя / Фамилия</th>
        <th>К оплате</th>
    </tr>
    </thead>
    <tbody>
    @foreach($periods as $period)
        <tr>
            <td>{{ $period->user->full_name }}</td>
            <td>{{ $period->payment }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
