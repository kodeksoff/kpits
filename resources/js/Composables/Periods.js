import axios from 'axios'
import {Inertia} from "@inertiajs/inertia";
import diabledButton from "lodash/seq";
import {useToast} from "vue-toastification"

export default function usePeriods(current_period, current_user, back_to) {

    const toast = useToast()

    const storePeriod = async () => {
        await axios.post(route('periods.working', current_period), {user_id: current_user})
            .then((response) => {
                toast.success('Период отправлен работнику')
                Inertia.visit(back_to, {reporting_period: current_period, user: current_user})
            })
            .catch((error) => {
                toast.error('Ошибка')
                console.log(error)
            })
    }
    const backToPlanning = async () => {
        await axios.post(route('periods.planning', current_period), {user_id: current_user})
            .then((response) => {
                toast.success('Период возвращен в планирование')
                Inertia.visit(back_to, {reporting_period: current_period, user: current_user})
            })
            .catch((error) => {
                toast.error('Ошибка')
                console.log(error)
            })
    }

    const approvedPeriod = async () => {
        await axios.post(route('periods.approved', current_period), {user_id: current_user})
            .then((response) => {
                toast.success('Период утвержден')
                Inertia.visit(back_to, {reporting_period: current_period, user: current_user})
            })
            .catch((error) => {
                toast.error('Ошибка')
                console.log(error)
            })
    }

    return {
        storePeriod,
        backToPlanning,
        approvedPeriod
    }
}
