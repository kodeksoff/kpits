<?php

namespace App\Traits;

use App\Observers\CacheObserver;

trait ObservableCache
{
    public static function bootObservableCache(): void
    {
        static::observe(CacheObserver::class);
    }
}
