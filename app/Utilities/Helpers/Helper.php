<?php

namespace App\Utilities\Helpers;

class Helper
{
    /**
     * @param array $array
     * @param $parent_id
     * @return array
     */
    public static function parseArrayTree(array $array, $parent_id = null): array
    {
        $return = [];
        foreach ($array as $item) {
            $root = [];
            if (isset($item['children'])) {
                $root = self::parseArrayTree($item['children'], $item['id']);
            }
            $return[] = ['id' => $item['id'], 'parent_id' => $parent_id];
            $return = array_merge($return, $root);
        }

        return $return;
    }
}
