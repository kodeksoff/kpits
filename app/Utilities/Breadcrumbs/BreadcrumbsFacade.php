<?php

namespace App\Utilities\Breadcrumbs;

use Illuminate\Support\Facades\Facade;

final class BreadcrumbsFacade extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return 'breadcrumbs';
    }
}
