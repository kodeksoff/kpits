<?php

namespace App\Utilities\Breadcrumbs;

class BreadcrumbsUtility
{
    protected array $breadcrumbs;

    public function __construct(string $title = 'Дашборд')
    {
        $this->set($title, route('index'));
    }

    public function set(string $title, string $link): static
    {
        $this->breadcrumbs[] = ['title' => $title, 'link' => $link];

        return $this;
    }

    public function get(): array
    {
        return $this->breadcrumbs;
    }
}
