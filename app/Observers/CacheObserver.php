<?php

namespace App\Observers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class CacheObserver
{
    /**
     * @param  Cache  $cache
     */
    public function __construct(private readonly Cache $cache)
    {
    }

    /**
     * @param  Model  $model
     * @return void
     */
    public function created(Model $model): void
    {
        $this->clearCache($model->getTable());
    }

    /**
     * @param  Model  $model
     * @return void
     */
    public function updated(Model $model): void
    {
        $this->clearCache($model->getTable());
    }

    /**
     * @param  Model  $model
     * @return void
     */
    public function deleted(Model $model): void
    {
        $this->clearCache($model->getTable());
    }

    /**
     * @param  Model  $model
     * @return void
     */
    public function restored(Model $model): void
    {
        $this->clearCache($model->getTable());
    }

    /**
     * @param  Model  $model
     * @return void
     */
    public function forceDeleted(Model $model): void
    {
        $this->clearCache($model->getTable());
    }

    /**
     * @param string $table
     * @return void
     */
    private function clearCache(string $table): void
    {
        $this->cache::tags([$table])->flush();
    }
}
