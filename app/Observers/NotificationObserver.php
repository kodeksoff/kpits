<?php

namespace App\Observers;

use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Support\Facades\Cache;

class NotificationObserver
{
    /**
     * @param  Cache  $cache
     */
    public function __construct(private readonly Cache $cache)
    {
    }

    /**
     * @param DatabaseNotification $model
     * @return void
     */
    public function created(DatabaseNotification $model): void
    {
        $this->clearCache($model->getTable());
    }

    /**
     * @param DatabaseNotification $model
     * @return void
     */
    public function updated(DatabaseNotification $model): void
    {
        $this->clearCache($model->getTable());
    }

    /**
     * @param DatabaseNotification $model
     * @return void
     */
    public function deleted(DatabaseNotification $model): void
    {
        $this->clearCache($model->getTable());
    }

    /**
     * @param DatabaseNotification $model
     * @return void
     */
    public function restored(DatabaseNotification $model): void
    {
        $this->clearCache($model->getTable());
    }

    /**
     * @param DatabaseNotification $model
     * @return void
     */
    public function forceDeleted(DatabaseNotification $model): void
    {
        $this->clearCache($model->getTable());
    }

    /**
     * @param string $table
     * @return void
     */
    private function clearCache(string $table): void
    {
        $this->cache::tags([$table])->flush();
    }
}
