<?php

namespace App\Observers;

use App\Models\TasksElements\ElementType;

class ElementTypeObserver
{
    /**
     * Handle the ElementType "created" event.
     *
     * @param  ElementType  $elementType
     * @return void
     */
    public function created(ElementType $elementType): void
    {
        $elementType->payment()->create(
            [
                'user_reporting_period_id' => $elementType->taskElement->task->userReportingPeriod->id,
                'price' => $elementType->price,
                'counting' => $elementType->completed,
            ]
        );
    }

    /**
     * Handle the ElementType "updated" event.
     *
     * @param  ElementType  $elementType
     * @return void
     */
    public function updated(ElementType $elementType): void
    {
        $elementType->payment()->updateOrCreate(
            [
                'user_reporting_period_id' => $elementType->taskElement->task->userReportingPeriod->id
            ],
            [
                'price' => $elementType->price,
                'counting' => $elementType->completed
            ]
        );
    }

    /**
     * Handle the ElementType "deleted" event.
     *
     * @param  ElementType  $elementType
     * @return void
     */
    public function deleted(ElementType $elementType): void
    {
        $elementType->payment()->delete();
    }

    /**
     * Handle the ElementType "restored" event.
     *
     * @param  ElementType  $elementType
     * @return void
     */
    public function restored(ElementType $elementType): void
    {
        //
    }

    /**
     * Handle the ElementType "force deleted" event.
     *
     * @param  ElementType  $elementType
     * @return void
     */
    public function forceDeleted(ElementType $elementType): void
    {
        //
    }
}
