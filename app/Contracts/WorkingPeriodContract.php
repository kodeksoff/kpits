<?php

namespace App\Contracts;

use App\Models\ReportingPeriod;

interface WorkingPeriodContract
{
    public function __invoke(array $data, ReportingPeriod $reporting_period);
}
