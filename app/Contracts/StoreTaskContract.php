<?php

namespace App\Contracts;

use App\Models\Task;

interface StoreTaskContract
{
    /**
     * @param StoreTaskCommentContract $taskComment
     * @param StoreTaskElementsContract $taskElements
     */
    public function __construct(
        StoreTaskCommentContract $taskComment,
        StoreTaskElementsContract $taskElements
    );

    /**
     * @param  array  $input
     * @return Task
     */
    public function __invoke(array $input): Task;
}
