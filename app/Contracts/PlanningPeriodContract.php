<?php

namespace App\Contracts;

use App\Models\ReportingPeriod;

interface PlanningPeriodContract
{
    public function __invoke(array $data, ReportingPeriod $reporting_period);
}
