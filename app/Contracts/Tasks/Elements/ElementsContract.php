<?php

namespace App\Contracts\Tasks\Elements;

interface ElementsContract
{
    public function __invoke(array $input);
}
