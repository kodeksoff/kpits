<?php

namespace App\Contracts\Tasks\Elements;

use App\Models\Tasks\TaskType;
use App\Models\TasksElements\ElementFormula;

interface StoreFormulasContract
{
    public function __invoke(ElementFormula $formula, TaskType $taskable, array $input);
}
