<?php

namespace App\Contracts\Tasks\Elements;

use App\Contracts\Tasks\Taskables\UpdateTaskFormulaStatusContract;
use App\Contracts\Tasks\Taskables\UpdateTaskSwitchesStatusContract;
use App\Models\Tasks\TaskType;
use App\Models\TasksElements\ElementFormula;

interface UpdateFormulasContract
{
    public function __construct(UpdateTaskFormulaStatusContract $switchesStatus);

    public function __invoke(ElementFormula $formula, TaskType $taskable, array $input);
}
