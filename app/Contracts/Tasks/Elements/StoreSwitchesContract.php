<?php

namespace App\Contracts\Tasks\Elements;

use App\Contracts\Tasks\Taskables\UpdateTaskSwitchesStatusContract;
use App\Models\Tasks\TaskType;
use App\Models\TasksElements\ElementSwitch;

interface StoreSwitchesContract
{
    public function __construct(UpdateTaskSwitchesStatusContract $switchesStatus);

    public function __invoke(ElementSwitch $switch, TaskType $taskable, array $input);
}
