<?php

namespace App\Contracts\Tasks\Elements;

use App\Contracts\Tasks\Taskables\UpdateTaskCheckListsStatusContract;
use App\Models\Tasks\TaskType;
use App\Models\TasksElements\ElementCheckList;

interface UpdateCheckListsContract
{
    public function __construct(UpdateTaskCheckListsStatusContract $checkListsStatus);

    public function __invoke(ElementCheckList $checkList, TaskType $taskable, array $input);
}
