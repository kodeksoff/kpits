<?php

namespace App\Contracts\Tasks\Elements;

use App\Models\Tasks\TaskType;
use App\Models\TasksElements\ElementPreconfigured;

interface StorePreconfiguredsContract
{
    public function __invoke(ElementPreconfigured $preconfigured, TaskType $taskable, array $input): static;
}
