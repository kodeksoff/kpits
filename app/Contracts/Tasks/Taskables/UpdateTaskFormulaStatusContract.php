<?php

namespace App\Contracts\Tasks\Taskables;

use App\Models\Tasks\TaskType;

interface UpdateTaskFormulaStatusContract
{
    public function __invoke(TaskType $taskable, array $input);
}
