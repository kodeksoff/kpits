<?php

namespace App\Contracts\Tasks\Taskables;

use App\Models\Tasks\TaskType;

interface UpdateTaskCheckListsStatusContract
{
    public function __invoke(TaskType $taskable, array $input);
}
