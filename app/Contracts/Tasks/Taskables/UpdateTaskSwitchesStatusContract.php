<?php

namespace App\Contracts\Tasks\Taskables;

use App\Models\Tasks\TaskType;

interface UpdateTaskSwitchesStatusContract
{
    public function __invoke(TaskType $taskable, array $input);
}
