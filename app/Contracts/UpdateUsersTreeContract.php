<?php

namespace App\Contracts;

interface UpdateUsersTreeContract
{
    public function __invoke(array $input);
}
