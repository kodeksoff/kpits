<?php

namespace App\Contracts;

use App\Actions\Tasks\StoreTaskElements;
use App\Models\Task;

interface UpdateTaskContract
{
    /**
     * @param StoreTaskCommentContract $taskComment
     * @param StoreTaskElements $taskElements
     */
    public function __construct(
        StoreTaskCommentContract  $taskComment,
        StoreTaskElements $taskElements
    );

    /**
     * @param Task $task
     * @param array $input
     * @return Task
     */
    public function __invoke(Task $task, array $input): Task;
}
