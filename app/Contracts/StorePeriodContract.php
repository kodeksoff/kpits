<?php

namespace App\Contracts;

interface StorePeriodContract
{
    public function __invoke(array $data);
}
