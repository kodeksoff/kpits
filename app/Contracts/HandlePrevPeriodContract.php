<?php

namespace App\Contracts;

use App\Models\UserReportingPeriod;

interface HandlePrevPeriodContract
{
    public function __invoke(UserReportingPeriod $new_period, int $user_id);
}
