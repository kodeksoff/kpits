<?php

namespace App\Contracts;

use App\Models\Task;

interface UpdateTaskStatusContract
{
    /**
     * @param Task $task
     * @param array $elements
     * @return void
     */
    public function __invoke(Task $task, array $elements): void;
}
