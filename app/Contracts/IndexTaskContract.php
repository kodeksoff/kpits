<?php

namespace App\Contracts;

use App\Http\Requests\IndexTaskRequest;
use App\Models\ReportingPeriod;
use Illuminate\Database\Eloquent\Collection;

interface IndexTaskContract
{
    /**
     * @param  IndexTaskRequest  $request
     * @return $this
     */
    public function handle(IndexTaskRequest $request): static;

    /**
     * @return Collection
     */
    public function getReportingPeriods(): Collection;

    /**
     * @return ?ReportingPeriod
     */
    public function getPeriod(): ?ReportingPeriod;

    /**
     * @return int|null
     */
    public function getCurrentPeriod(): int|null;
}
