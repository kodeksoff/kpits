<?php

namespace App\Contracts;

use App\Models\UserReportingPeriod;

interface ApprovePeriodContract
{
    public function __invoke(UserReportingPeriod $userReportingPeriod);
}
