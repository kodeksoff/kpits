<?php

namespace App\Contracts;

use App\Models\Task;

interface UpdateTaskElementsContract
{
    /**
     * @param  Task  $task
     * @param  array  $input
     * @return Task
     */
    public function __invoke(Task $task, array $input): Task;
}
