<?php

namespace App\Contracts;

use App\Models\Task;

interface StoreTaskCommentContract
{
    public function __invoke(Task $task, string $comment);
}
