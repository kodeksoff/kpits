<?php

namespace App\Actions\Tasks\Elements;

use App\Contracts\Tasks\Elements\StorePreconfiguredsContract;
use App\Models\Tasks\TaskType;
use App\Models\TasksElements\ElementPreconfigured;
use FormulaParser\FormulaParser;

class StorePreconfigureds extends ElementsStore implements StorePreconfiguredsContract
{
    public function __invoke(ElementPreconfigured $preconfigured, TaskType $taskable, array $input): static
    {
        $elements = [];
        foreach ($input as $key => $element) {
            $elements[] = [
                'id' => $element['id'],
                'task_preconfigured_id' => $taskable->id,
                'completed' => 0,
                'order' => $key,
                'type' => $element['type']
            ];

            $type_config = config('predefinedformulas.' . $element['type'] . '.fields');
            $data = [];
            foreach ($type_config as $item) {
                $data['fields'][$item['key']] = $element['input']['fields'][$item['key']] ?? 0;
            }

            $preconfigured->updateOrCreate(
                [
                    'task_preconfigured_id' => $taskable->id,
                ],
                [
                    'task_preconfigured_id' => $taskable->id,
                    'completed' => 0,
                    'price' => 0,
                    'order' => $key,
                    'type' => $element['type'],
                    'input' => $data
                ]
            );
        }

        return $this;
    }
}
