<?php

namespace App\Actions\Tasks\Elements;

use App\Contracts\Tasks\Elements\UpdateCheckListsContract;
use App\Contracts\Tasks\Taskables\UpdateTaskCheckListsStatusContract;
use App\Models\Tasks\TaskType;
use App\Models\TasksElements\ElementCheckList;

class UpdateCheckLists extends ElementsUpdate implements UpdateCheckListsContract
{
    /**
     * @param UpdateTaskCheckListsStatusContract $checkListsStatus
     */
    public function __construct(private readonly UpdateTaskCheckListsStatusContract $checkListsStatus)
    {
    }

    /**
     * @param ElementCheckList $checkList
     * @param TaskType $taskable
     * @param array $input
     * @return $this
     */
    public function __invoke(ElementCheckList $checkList, TaskType $taskable, array $input): static
    {
        \Log::info('f', $input);
        $completed = [];
        foreach ($input as $key => $element) {
            $completed[] = [
                'id' => $element['id'],
                'task_check_list_id' => $taskable->id,
                'title' => $element['title'],
                'completed' => $element['completed'],
                'price' => $element['price'],
                'order' => $key
            ];

            $checkList->where('id', $element['id'])
                ->where('task_check_list_id', $taskable->id)
                ->first()
                ->update(
                    [
                        'title' => $element['title'],
                        'task_check_list_id' => $taskable->id,
                        'completed' => $element['completed'],
                        'price' => $element['price'],
                        'order' => $key
                    ]
                );
        }

        ($this->checkListsStatus)($taskable, $completed);

        return $this;
    }
}
