<?php

namespace App\Actions\Tasks\Elements;

use App\Contracts\Tasks\Elements\UpdateSwitchesContract;
use App\Contracts\Tasks\Taskables\UpdateTaskSwitchesStatusContract;
use App\Models\Tasks\TaskType;
use App\Models\TasksElements\ElementSwitch;

class UpdateSwitches extends ElementsUpdate implements UpdateSwitchesContract
{
    /**
     * @param UpdateTaskSwitchesStatusContract $switchesStatus
     */
    public function __construct(private readonly UpdateTaskSwitchesStatusContract $switchesStatus)
    {
    }

    /**
     * @param ElementSwitch $switch
     * @param TaskType $taskable
     * @param array $input
     * @return $this
     */
    public function __invoke(ElementSwitch $switch, TaskType $taskable, array $input): static
    {
        $elements = [];
        foreach ($input as $key => $element) {
            $elements[] = [
                'id' => $element['id'],
                'task_switch_id' => $taskable->id,
                'title' => $element['title'],
                'completed' => $element['completed'],
                'price' => $element['price'],
                'order' => $key
            ];

            $switch->where('id', $element['id'])
                ->where('task_switch_id', $taskable->id)
                ->first()
                ->update(
                    [
                        'title' => $element['title'],
                        'task_switch_id' => $taskable->id,
                        'completed' => $element['completed'],
                        'price' => $element['price'],
                        'order' => $key
                    ]
                );
        }

        ($this->switchesStatus)($taskable, $elements);

        return $this;
    }
}
