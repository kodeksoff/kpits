<?php

namespace App\Actions\Tasks\Elements;

use App\Contracts\Tasks\Elements\UpdatePreconfiguredsContract;
use App\Models\Tasks\TaskType;
use App\Models\TasksElements\ElementPreconfigured;
use App\Models\TasksElements\ElementSwitch;
use FormulaParser\FormulaParser;

class UpdatePreconfigureds extends ElementsUpdate implements UpdatePreconfiguredsContract
{
    public function __invoke(ElementPreconfigured $preconfigured, TaskType $taskable, array $input): static
    {
        $elements = [];
        foreach ($input as $key => $element) {
            $formula_parser = new FormulaParser(str_replace('x', $element['result'], $element['formula']));

            $elements[] = [
                'id' => $element['id'],
                'task_preconfigured_id' => $taskable->id,
                'title' => $element['title'],
                'completed' => $element['result'] !== 0 ? 1 : 0,
                'order' => $key
            ];

            $preconfigured->updateOrCreate(
                [
                    'task_preconfigured_id' => $taskable->id,
                ],
                [
                    'task_preconfigured_id' => $taskable->id,
                    'result' => $element['result'],
                    'price' => $formula_parser->getResult()[1],
                    'completed' => $element['result'] !== 0 ? 1 : 0,
                    'order' => $key,
                    'type' => $element['type']
                ]
            );
        }

        return $this;
    }
}
