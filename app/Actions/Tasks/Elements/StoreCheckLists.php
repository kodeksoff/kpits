<?php

namespace App\Actions\Tasks\Elements;

use App\Contracts\Tasks\Elements\StoreCheckListsContract;
use App\Contracts\Tasks\Taskables\UpdateTaskCheckListsStatusContract;
use App\Models\Tasks\TaskType;
use App\Models\TasksElements\ElementCheckList;

class StoreCheckLists extends ElementsStore implements StoreCheckListsContract
{
    /**
     * @param UpdateTaskCheckListsStatusContract $checkListsStatus
     */
    public function __construct(private readonly  UpdateTaskCheckListsStatusContract $checkListsStatus)
    {
    }

    public function __invoke(ElementCheckList $checkList, TaskType $taskable, array $input): static
    {
        $completed = [];
        foreach ($input as $key => $element) {
            $completed[] = [
                'task_check_list_id' => $taskable->id,
                'title' => $element['title'],
                'completed' => $element['completed'],
                'price' => $element['price'],
                'order' => $key
            ];

            $checkList->create([
                'task_check_list_id' => $taskable->id,
                'title' => $element['title'],
                'completed' => $element['completed'],
                'price' => $element['price'],
                'order' => $key
            ]);
        }

        ($this->checkListsStatus)($taskable, $completed);

        return $this;
    }
}
