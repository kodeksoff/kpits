<?php

namespace App\Actions\Tasks\Elements;

use App\Contracts\Tasks\Elements\StoreFormulasContract;
use App\Models\Tasks\TaskType;
use App\Models\TasksElements\ElementFormula;

class StoreFormulas extends ElementsStore implements StoreFormulasContract
{
    public function __invoke(ElementFormula $formula, TaskType $taskable, array $input): static
    {
        $completed = [];
        foreach ($input as $key => $element) {
            $completed[] = [
                'task_formula_id' => $taskable->id,
                'title' => $element['title'],
                'completed' => $element['completed'],
                'price' => $element['price'],
                'order' => $key
            ];

            $formula->create(
                [
                    'formula' => $element['formula'],
                    'task_formula_id' => $taskable->id,
                    'price' => 0,
                    'completed' => 0,
                    'order' => $key,
                ]
            );
        }


        return $this;
    }
}
