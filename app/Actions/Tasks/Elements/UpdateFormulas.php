<?php

namespace App\Actions\Tasks\Elements;

use App\Contracts\Tasks\Elements\UpdateFormulasContract;
use App\Contracts\Tasks\Taskables\UpdateTaskFormulaStatusContract;
use App\Models\TasksElements\ElementFormula;
use App\Models\Tasks\TaskType;
use FormulaParser\FormulaParser;

class UpdateFormulas extends ElementsUpdate implements UpdateFormulasContract
{

    public function __construct(private readonly UpdateTaskFormulaStatusContract $formulaStatus)
    {
    }

    public function __invoke(ElementFormula $formula, TaskType $taskable, array $input): static
    {
        $elements = [];
        foreach ($input as $key => $element) {
            $formula_parser = new FormulaParser(str_replace('x', $element['result'], $element['formula']));

            $elements[] = [
                'id' => $element['id'],
                'task_formula_id' => $taskable->id,
                'title' => $element['title'],
                'completed' => $element['result'] !== 0 ? 1 : 0,
                'order' => $key
            ];

            $formula->updateOrCreate(
                [
                    'task_formula_id' => $taskable->id,
                ],
                [
                    'task_formula_id' => $taskable->id,
                    'formula' => $element['formula'],
                    'result' => $element['result'],
                    'price' => $formula_parser->getResult()[1],
                    'completed' => $element['result'] !== 0 ? 1 : 0 ,
                    'order' => $key
                ]
            );
        }

        ($this->formulaStatus)($taskable, $elements);

        return $this;
    }
}
