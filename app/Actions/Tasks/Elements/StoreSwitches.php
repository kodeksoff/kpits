<?php

namespace App\Actions\Tasks\Elements;

use App\Contracts\Tasks\Elements\StoreSwitchesContract;
use App\Contracts\Tasks\Taskables\UpdateTaskSwitchesStatusContract;
use App\Models\Tasks\TaskType;
use App\Models\TasksElements\ElementSwitch;

class StoreSwitches extends ElementsStore implements StoreSwitchesContract
{
    /**
     * @param UpdateTaskSwitchesStatusContract $switchesStatus
     */
    public function __construct(private readonly UpdateTaskSwitchesStatusContract $switchesStatus)
    {
    }

    public function __invoke(ElementSwitch $switch, TaskType $taskable, array $input): static
    {
        $completed = [];
        foreach ($input as $key => $element) {
            $completed[] = [
                'task_switch_id' => $taskable->id,
                'title' => $element['title'],
                'completed' => $element['completed'],
                'price' => $element['price'],
                'order' => $key
            ];

            $switch->create(
                [
                    'title' => $element['title'],
                    'task_switch_id' => $taskable->id,
                    'completed' => $element['completed'],
                    'price' => $element['price'],
                    'order' => $key
                ]
            );
        }

        ($this->switchesStatus)($taskable, $completed);

        return $this;
    }
}
