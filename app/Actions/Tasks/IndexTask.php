<?php

namespace App\Actions\Tasks;

use App\Contracts\IndexTaskContract;
use App\Http\Requests\IndexTaskRequest;
use App\Models\ReportingPeriod;
use Breadcrumbs;
use Illuminate\Database\Eloquent\Collection;

/**
 *
 */
class IndexTask implements IndexTaskContract
{
    /**
     * @var Collection
     */
    protected Collection $reporting_periods;


    /**
     * @var ?ReportingPeriod
     */
    protected ?ReportingPeriod $period;

    /**
     * @var int|null
     */
    protected int|null $current_period;

    /**
     * @param  IndexTaskRequest  $request
     * @return $this
     */
    public function handle(IndexTaskRequest $request): static
    {
        Breadcrumbs::set('Мои задачи', route('tasks.index'));

        $this->current_period = $request->reporting_period ?? ReportingPeriod::currentPeriod()->id;

        $this->reporting_periods = ReportingPeriod::availablePeriods();

        $this->period = ReportingPeriod::withUserTasks($request->reporting_period)->first();

        return $this;
    }

    /**
     * @return Collection
     */
    public function getReportingPeriods(): Collection
    {
        return $this->reporting_periods;
    }


    /**
     * @return ?ReportingPeriod
     */
    public function getPeriod(): ?ReportingPeriod
    {
        return $this->period;
    }

    /**
     * @return int|null
     */
    public function getCurrentPeriod(): int|null
    {
        return $this->current_period;
    }
}
