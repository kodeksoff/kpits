<?php

namespace App\Actions\Tasks;

use App\Contracts\UpdateTaskStatusContract;
use App\Enums\TaskElementsType;
use App\Models\Task;

class UpdateTaskStatus implements UpdateTaskStatusContract
{
    public function __invoke(Task $task, array $elements): void
    {
        $task_status = 'partially-completed';
        $element_count = count($elements);
        $compelted_count = 0;

        foreach ($elements as $key => $element) {
            if ($elements[0]['type'] === TaskElementsType::radio->value) {
                if ($key === array_key_first($elements) && (int) $element['completed'] === 1) {
                    $task_status = 'not-completed';
                }
                if ($key === array_key_last($elements) && (int) $element['completed'] === 1) {
                    $task_status = 'completed';
                }
            }

            if ($elements[0]['type'] === TaskElementsType::checkbox->value) {
                $task_status = 'not-completed';
                if ((int) $element['completed'] === 1) {
                    $compelted_count++;
                }

                if ($compelted_count === $element_count) {
                    $task_status = 'completed';
                }
            }
        }

        $task->update(['status' => $task_status]);
    }
}
