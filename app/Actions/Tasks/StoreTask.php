<?php

namespace App\Actions\Tasks;

use App\Contracts\StoreTaskCommentContract;
use App\Contracts\StoreTaskContract;
use App\Contracts\StoreTaskElementsContract;
use App\Models\Task;
use Illuminate\Support\Facades\DB;
use Throwable;

class StoreTask implements StoreTaskContract
{
    /**
     * @param StoreTaskCommentContract $taskComment
     * @param StoreTaskElementsContract $taskElements
     */
    public function __construct(
        protected StoreTaskCommentContract $taskComment,
        protected StoreTaskElementsContract $taskElements
    ) {
    }

    /**
     * @param  array  $input
     * @return Task
     *
     * @throws Throwable
     */
    public function __invoke(array $input): Task
    {
        return DB::transaction(function () use ($input) {
            $task = Task::create($input['task']);

            if (array_key_exists('comment', $input) && $input['comment']) {
                ($this->taskComment)($task, $input['comment']);
            }

            $task->taskable->elements()->delete();

            ($this->taskElements)($task, $input['task']['elements']);

            return $task;
        });
    }
}
