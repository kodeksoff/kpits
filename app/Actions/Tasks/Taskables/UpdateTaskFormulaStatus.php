<?php

namespace App\Actions\Tasks\Taskables;

use App\Contracts\Tasks\Taskables\UpdateTaskFormulaStatusContract;
use App\Models\Tasks\TaskType;

class UpdateTaskFormulaStatus implements UpdateTaskFormulaStatusContract
{
    public function __invoke(TaskType $taskable, array $input)
    {
        $task_status = 'not-completed';

        foreach ($input as $element) {
            if ($element['completed'] === 1) {
                $task_status = 'completed';
            }
        }

        $taskable->update(['status' => $task_status]);
    }
}
