<?php

namespace App\Actions\Tasks\Taskables;

use App\Contracts\Tasks\Taskables\UpdateTaskCheckListsStatusContract;
use App\Models\Tasks\TaskType;

class UpdateTaskCheckListsStatus implements UpdateTaskCheckListsStatusContract
{
    public function __invoke(TaskType $taskable, array $input)
    {
        $task_status = 'not-completed';
        $element_count = count($input);
        $compelted_count = 0;

        foreach ($input as  $element) {
            if ((int)$element['completed'] === 1) {
                $compelted_count++;
            }

            if ($compelted_count === $element_count) {
                $task_status = 'completed';
            }
        }

        $taskable->update(['status' => $task_status]);
    }
}
