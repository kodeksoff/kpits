<?php

namespace App\Actions\Tasks\Taskables;

use App\Contracts\Tasks\Taskables\UpdateTaskSwitchesStatusContract;
use App\Models\Tasks\TaskType;

class UpdateTaskSwitchesStatus implements UpdateTaskSwitchesStatusContract
{
    public function __invoke(TaskType $taskable, array $input)
    {
        $task_status = 'partially-completed';

        foreach ($input as $key => $element) {
            if ($key === array_key_first($input) && (int)$element['completed'] === 1) {
                $task_status = 'not-completed';
            }
            if ($key === array_key_last($input) && (int)$element['completed'] === 1) {
                $task_status = 'completed';
            }
        }

        $taskable->update(['status' => $task_status]);
    }
}
