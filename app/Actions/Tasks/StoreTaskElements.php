<?php

namespace App\Actions\Tasks;

use App\Contracts\StoreTaskElementsContract;
use App\Contracts\UpdateTaskStatusContract;
use App\Models\Task;

class StoreTaskElements implements StoreTaskElementsContract
{
    public function __construct(protected UpdateTaskStatusContract $updateTaskStatus)
    {
    }

    public function __invoke(Task $task, array $input): Task
    {
        $task->taskable
            ->elements()
            ->getRelated()
            ->handleStore($task->taskable, $input);

        return $task;
    }
}
