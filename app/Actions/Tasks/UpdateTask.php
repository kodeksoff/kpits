<?php

namespace App\Actions\Tasks;

use App\Contracts\StoreTaskCommentContract;
use App\Contracts\StoreTaskElementsContract;
use App\Contracts\UpdateTaskContract;
use App\Models\Task;
use Illuminate\Support\Facades\DB;
use Throwable;

class UpdateTask implements UpdateTaskContract
{
    /**
     * @param StoreTaskCommentContract $taskComment
     * @param StoreTaskElements $taskElements
     */
    public function __construct(
        protected StoreTaskCommentContract  $taskComment,
        protected StoreTaskElementsContract $taskElements
    ) {
    }

    /**
     * @param Task $task
     * @param array $input
     * @return Task
     *
     * @throws Throwable
     */
    public function __invoke(Task $task, array $input): Task
    {
        return DB::transaction(function () use ($task, $input) {
            $task->taskable()->update([
                'title' => $input['task']['title'],
                'description' => $input['task']['description'],
                'type' => $input['task']['type'],
            ]);

            if ($input['comment']) {
                ($this->taskComment)($task, $input['comment']);
            }

            $task->taskable->elements->each->delete();

            ($this->taskElements)($task, $input['task']['elements']);

            return $task;
        });
    }
}
