<?php

namespace App\Actions\Tasks;

use App\Contracts\StoreTaskCommentContract;
use App\Models\Task;

class StoreTaskComment implements StoreTaskCommentContract
{
    public function __invoke(Task $task, string $comment)
    {
        $task->comment()->updateOrCreate(['task_id' => $task->id], ['comment' => $comment]);
    }
}
