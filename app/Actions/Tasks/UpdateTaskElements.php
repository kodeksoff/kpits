<?php

namespace App\Actions\Tasks;

use App\Contracts\StoreTaskCommentContract;
use App\Contracts\UpdateTaskElementsContract;
use App\Enums\UserPeriodStatus;
use App\Models\Task;
use Illuminate\Support\Facades\DB;
use Throwable;

class UpdateTaskElements implements UpdateTaskElementsContract
{
    public function __construct(
        protected StoreTaskCommentContract $taskComment,
    ) {
    }

    /**
     * @param Task $task
     * @param array $input
     * @return Task
     *
     * @throws Throwable
     */
    public function __invoke(Task $task, array $input): Task
    {
        abort_if(
            $task->userReportingPeriod->status !== UserPeriodStatus::in_work,
            403,
            'Не валидная попытка сохранения задачи'
        );

        return DB::transaction(function () use ($input, $task) {
            $task->taskable
                ->elements()
                ->getRelated()
                ->handleUpdate($task->taskable, $input['elements']);

            if ($input['comment']) {
                ($this->taskComment)($task, $input['comment']);
            }

            return $task;
        }, 5);
    }
}
