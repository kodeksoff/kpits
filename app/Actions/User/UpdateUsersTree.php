<?php

namespace App\Actions\User;

use App\Contracts\UpdateUsersTreeContract;
use App\Models\User;
use App\Utilities\Helpers\Helper;
use Exception;

class UpdateUsersTree implements UpdateUsersTreeContract
{
    /**
     * @throws Exception
     */
    public function __invoke(array $input)
    {
        $data = Helper::parseArrayTree($input['data']);

        foreach ($data as $key => $item) {
            if ($item['id'] !== 1 && $item['parent_id'] === null) {
                throw new Exception('Возможен только один корневой элемент', 422);
            }

            User::find($item['id'])->update(['supervisor_id' => $item['parent_id']]);
        }
    }
}
