<?php

namespace App\Actions\Periods;

use App\Contracts\HandlePrevPeriodContract;
use App\Contracts\StorePeriodContract;
use App\Enums\UserPeriodStatus;
use App\Models\ReportingPeriod;
use Illuminate\Support\Facades\DB;
use Throwable;

class StorePeriodAction implements StorePeriodContract
{
    public function __construct(private readonly HandlePrevPeriodContract $prevPeriod)
    {
    }

    /**
     * @throws Throwable
     */
    public function __invoke(array $data)
    {
        DB::transaction(function () use ($data) {
            $new_period = ReportingPeriod::find($data['reporting_period']);

            $new_user_period = $new_period->userPeriod()->create([
                'user_id' => $data['user_id'],
                'status' => UserPeriodStatus::planning
            ]);

            ($this->prevPeriod)($new_user_period, $data['user_id']);
        });
    }
}
