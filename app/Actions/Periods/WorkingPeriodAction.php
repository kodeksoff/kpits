<?php

namespace App\Actions\Periods;

use App\Contracts\WorkingPeriodContract;
use App\Enums\UserPeriodStatus;
use App\Events\UserReportingPeriodInWork;
use App\Models\ReportingPeriod;

class WorkingPeriodAction implements WorkingPeriodContract
{
    public function __invoke(array $data, ReportingPeriod $reporting_period)
    {
        $period = $reporting_period->userPeriods()->updateOrCreate(
            [
                'reporting_period_id' => $reporting_period->id,
                'user_id' => $data['user_id']
            ],
            [
                'reporting_period_id' => $reporting_period->id,
                'user_id' =>  $data['user_id'],
                'status' => UserPeriodStatus::in_work
            ]
        );

        UserReportingPeriodInWork::dispatch($period);
    }
}
