<?php

namespace App\Actions\Periods;

use App\Contracts\PlanningPeriodContract;
use App\Enums\UserPeriodStatus;
use App\Models\ReportingPeriod;

class PlanningPeriodAction implements PlanningPeriodContract
{
    public function __invoke(array $data, ReportingPeriod $reporting_period)
    {
        $reporting_period->userPeriods()->updateOrCreate(
            [
                'reporting_period_id' => $reporting_period->id,
                'user_id' => $data['user_id']
            ],
            [
                'reporting_period_id' => $reporting_period->id,
                'user_id' =>  $data['user_id'],
                'status' => UserPeriodStatus::planning
            ]
        );
    }
}
