<?php

namespace App\Actions\Periods;

use App\Contracts\HandlePrevPeriodContract;
use App\Contracts\StoreTaskContract;
use App\Enums\TaskElementsType;
use App\Enums\TaskTypes;
use App\Models\UserReportingPeriod;

class HandlePrevPeriod implements HandlePrevPeriodContract
{
    /**
     * @param StoreTaskContract $storeTask
     */
    public function __construct(private readonly StoreTaskContract $storeTask)
    {
    }

    /**
     * @param UserReportingPeriod $new_period
     * @param int $user_id
     * @return void
     */
    public function __invoke(UserReportingPeriod $new_period, int $user_id): void
    {
        $prev_period = UserReportingPeriod::prevPeriod($user_id);

        if (!is_null($prev_period)) {
            $prev_period->tasks
                ->map(function ($task) use ($new_period) {
                    $task->user_reporting_period_id = $new_period->id;
                    return collect($task)->forget(['created_at', 'updated_at', 'to_pay'])->toArray();
                })
                ->each(function ($task) {
                    return match ($task['type']) {
                        TaskTypes::long_term->value => ($this->storeTask)($this->handleLongTermTask($task)),
                        TaskTypes::regular->value => ($this->storeTask)($this->handleRegularTask($task)),
                        TaskTypes::urgent->value => ($this->storeTask)($this->handleUrgentTask($task)),
                    };
                });
        }
    }

    /**
     * Сбрасываем стоимость для долгосрочных задач
     * @param array $task
     * @return array
     */
    private function handleLongTermTask(array $task): array
    {
        foreach ($task['elements'] as $key => $element) {
            unset($task['elements'][$key]['created_at']);
            unset($task['elements'][$key]['updated_at']);
            if ($element['completed'] === 1) {
                $task['elements'][$key]['price'] = 0;
            }
        }

        unset($task['comment']);

        $arr['task'] = $task;

        return $arr;
    }

    /**
     * Сбрасываем достижение целей для регулярных задач
     * @param array $task
     * @return array
     */
    private function handleRegularTask(array $task): array
    {
        foreach ($task['elements'] as $key => $element) {
            unset($task['elements'][$key]['created_at']);
            unset($task['elements'][$key]['updated_at']);
            $task['elements'][$key]['completed'] = 0;
        }

        unset($task['comment']);

        $arr['task'] = $task;

        return $arr;
    }

    /**
     * Срочные задачи
     * @param array $task
     * @return array|void
     */
    private function handleUrgentTask(array $task)
    {
        // Проверяем тип элементов: чекбокс
        if ($task['elements'][0]['type'] === TaskElementsType::checkbox->value) {
            $compelted = 0;
            $element_count = count($task['elements']);

            // Проверяем количество выполненых задач
            foreach ($task['elements'] as $key => $element) {
                unset($task['elements'][$key]['created_at']);
                unset($task['elements'][$key]['updated_at']);
                if ($element['completed'] === 1) {
                    $task['elements'][$key]['price'] = 0;
                    $compelted++;
                }
            }

            /*
             * Если количество элементов задачи не равно количеству выполненых задач
             * пушим в массив новых задач и
             * с обнуленным ценником выполенных элементов
             */
            if ($compelted !== $element_count) {
                $arr['task'] = $task;

                unset($task['comment']);

                return $arr;
            }
        }

        /*
         * Проверяем тип элементов: переключатель
         * Если последний элемент не выполнен
         * пушим в массив новых задач и
         * с обнуленным ценником выполенных элементов
         */
        if ($task['elements'][0]['type'] === TaskElementsType::radio->value) {
            foreach ($task['elements'] as $key => $element) {
                unset($task['elements'][$key]['created_at']);
                unset($task['elements'][$key]['updated_at']);
                if ($key === array_key_last($task['elements']) && $element['completed'] !== 1) {
                    foreach ($task['elements'] as $second_key => $item) {
                        if ($item['completed'] === 1) {
                            $task['elements'][$second_key]['price'] = 0;
                        }
                    }

                    unset($task['comment']);

                    $arr['task'] = $task;

                    return $arr;
                }
            }
        }
    }
}
