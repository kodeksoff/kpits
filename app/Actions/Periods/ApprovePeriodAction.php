<?php

namespace App\Actions\Periods;

use App\Contracts\ApprovePeriodContract;
use App\Enums\UserPeriodStatus;
use App\Events\UserReportingPeriodUnderReview;
use App\Models\UserReportingPeriod;

class ApprovePeriodAction implements ApprovePeriodContract
{
    public function __invoke(UserReportingPeriod $userReportingPeriod)
    {
        abort_if(
            $userReportingPeriod->status !== UserPeriodStatus::in_work,
            403,
            'Не валидная попытка сохранения периода'
        );

        $userReportingPeriod->update(['status' => UserPeriodStatus::under_review]);

        UserReportingPeriodUnderReview::dispatch($userReportingPeriod);
    }
}
