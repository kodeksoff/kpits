<?php

namespace App\Actions\Fortify;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Laravel\Fortify\Contracts\CreatesNewUsers;

class CreateNewUser implements CreatesNewUsers
{
    use PasswordValidationRules;

    /**
     * Validate and create a newly registered user.
     *
     * @param array $input
     * @return User
     */
    public function create(array $input): User
    {
        return User::create([
            'first_name' => $input['first_name'],
            'second_name' => $input['second_name'],
            'phone' => $input['phone'],
            'email' => $input['email'] ?? null,
            'role_id' => $input['role_id'] ?? null,
            'counting_id' => $input['counting_id'] ?? null,
            'supervisor_id' => $input['supervisor_id'] ?? null,
            'disabled' => $input['disabled'] ?? 0,
            'password' => Hash::make($input['password']),
        ]);
    }
}
