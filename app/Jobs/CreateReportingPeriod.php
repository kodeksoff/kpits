<?php

namespace App\Jobs;

use App\Models\ReportingPeriod;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CreateReportingPeriod implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(): void
    {
        $monthName = now()->addMonth()->monthName;
        $monthName = mb_strtoupper(mb_substr($monthName, 0, 1, 'UTF-8'), 'UTF-8') .
            mb_substr($monthName, 1, mb_strlen($monthName), 'UTF-8');

        ReportingPeriod::firstOrCreate(
            [
                'title' => $monthName . ' ' . now()->addMonth()->year,
                'start_period' => now()->addMonth()->startOfMonth(),
                'end_period' => now()->addMonth()->endOfMonth(),
            ],
            [
                'title' => $monthName . ' ' . now()->addMonth()->year,
                'start_period' => now()->addMonth()->startOfMonth(),
                'end_period' => now()->addMonth()->endOfMonth(),
            ]
        );
    }
}
