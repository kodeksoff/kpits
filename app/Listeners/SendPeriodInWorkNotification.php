<?php

namespace App\Listeners;

use App\Events\UserReportingPeriodInWork;
use App\Notifications\PeriodInWork;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendPeriodInWorkNotification implements ShouldQueue
{
    /**
     * @var string
     */
    public string $queue = 'default';

    /**
     * @var bool
     */
    public bool $afterCommit = true;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param UserReportingPeriodInWork $event
     * @return void
     */
    public function handle(UserReportingPeriodInWork $event): void
    {
        $event->reportingPeriod->user->notify(new PeriodInWork($event->reportingPeriod));
    }
}
