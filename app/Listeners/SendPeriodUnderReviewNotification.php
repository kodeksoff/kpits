<?php

namespace App\Listeners;

use App\Events\UserReportingPeriodUnderReview;
use App\Notifications\PeriodUnderReview;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendPeriodUnderReviewNotification implements ShouldQueue
{
    /**
     * @var string
     */
    public string $queue = 'default';

    /**
     * @var bool
     */
    public bool $afterCommit = true;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param UserReportingPeriodUnderReview $event
     * @return void
     */
    public function handle(UserReportingPeriodUnderReview $event): void
    {
        $event->reportingPeriod->user->supervisor->notify(new PeriodUnderReview($event->reportingPeriod));
    }
}
