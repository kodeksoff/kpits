<?php

namespace App\Providers;

use Breadcrumbs;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\ServiceProvider;
use Inertia\Inertia;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Inertia::share([
            'csrf_token' => function () {
                return csrf_token();
            },
            'auth.user' => function () {
                return Auth::user();
            },
            'breadcrumbs' => function () {
                return Breadcrumbs::get();
            },
            'toast' => function () {
                return Session::get('toast');
            },
            'notifications' => function () {
                return Cache::tags(['notifications', 'users', 'reporting_periods', 'user_reporting_periods'])
                    ->remember('notifications_' . Auth::user()?->id, 360 * 60, function () {
                        return Auth::user()->unreadNotifications ?? null;
                    });
            }
        ]);
    }
}
