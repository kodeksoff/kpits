<?php

namespace App\Providers;

use App\Events\UserReportingPeriodInWork;
use App\Events\UserReportingPeriodUnderReview;
use App\Listeners\SendPeriodInWorkNotification;
use App\Listeners\SendPeriodUnderReviewNotification;
use App\Models\TasksElements\ElementCheckList;
use App\Models\TasksElements\ElementFormula;
use App\Models\TasksElements\ElementSwitch;
use App\Observers\ElementTypeObserver;
use App\Observers\NotificationObserver;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Notifications\DatabaseNotification;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event to listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        UserReportingPeriodInWork::class => [
            SendPeriodInWorkNotification::class
        ],
        UserReportingPeriodUnderReview::class => [
            SendPeriodUnderReviewNotification::class
        ],
    ];

    protected $observers = [
        DatabaseNotification::class => [
            NotificationObserver::class
        ],
        ElementSwitch::class => [
            ElementTypeObserver::class
        ],
        ElementCheckList::class => [
            ElementTypeObserver::class
        ],
        ElementFormula::class => [
            ElementTypeObserver::class
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot(): void
    {
        //
    }

    /**
     * Determine if events and listeners should be automatically discovered.
     *
     * @return bool
     */
    public function shouldDiscoverEvents()
    {
        return false;
    }
}
