<?php

namespace App\Providers;

use App\Models\Task;
use App\Models\User;
use App\Policies\TaskPolicy;
use App\Policies\UserPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The model to policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        User::class => UserPolicy::class,
        Task::class => TaskPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('access-admin', function (User $user) {
            return $user->role->is_admin === 1;
        });

        Gate::define('access-manager', function (User $user) {
            return in_array($user->role_id, [1, 2, 3]);
        });

        Gate::define('access-accountant', function (User $user) {
            return $user->role_id === 4;
        });

        Gate::define('access-subordinates', function (User $user, int $user_id) {
            return in_array($user_id, $user->getSubordinatesIds());
        });
    }
}
