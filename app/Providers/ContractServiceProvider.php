<?php

namespace App\Providers;

use App\Actions\Periods\ApprovedPeriodAction;
use App\Actions\Periods\ApprovePeriodAction;
use App\Actions\Periods\HandlePrevPeriod;
use App\Actions\Periods\PlanningPeriodAction;
use App\Actions\Periods\StorePeriodAction;
use App\Actions\Periods\WorkingPeriodAction;
use App\Actions\Tasks\Elements\StoreCheckLists;
use App\Actions\Tasks\Elements\StoreFormulas;
use App\Actions\Tasks\Elements\StorePreconfigureds;
use App\Actions\Tasks\Elements\StoreSwitches;
use App\Actions\Tasks\Elements\UpdateCheckLists;
use App\Actions\Tasks\Elements\UpdateFormulas;
use App\Actions\Tasks\Elements\UpdatePreconfigureds;
use App\Actions\Tasks\Elements\UpdateSwitches;
use App\Actions\Tasks\IndexTask;
use App\Actions\Tasks\StoreTask;
use App\Actions\Tasks\StoreTaskComment;
use App\Actions\Tasks\StoreTaskElements;
use App\Actions\Tasks\Taskables\UpdateTaskCheckListsStatus;
use App\Actions\Tasks\Taskables\UpdateTaskFormulaStatus;
use App\Actions\Tasks\Taskables\UpdateTaskSwitchesStatus;
use App\Actions\Tasks\UpdateTask;
use App\Actions\Tasks\UpdateTaskElements;
use App\Actions\Tasks\UpdateTaskStatus;
use App\Actions\User\UpdateUsersTree;
use App\Contracts\ApprovedPeriodContract;
use App\Contracts\ApprovePeriodContract;
use App\Contracts\PlanningPeriodContract;
use App\Contracts\StorePeriodContract;
use App\Contracts\StoreTaskCommentContract;
use App\Contracts\Tasks\Elements\StoreCheckListsContract;
use App\Contracts\Tasks\Elements\StoreFormulasContract;
use App\Contracts\Tasks\Elements\StorePreconfiguredsContract;
use App\Contracts\Tasks\Elements\StoreSwitchesContract;
use App\Contracts\Tasks\Elements\UpdateCheckListsContract;
use App\Contracts\Tasks\Elements\UpdateFormulasContract;
use App\Contracts\Tasks\Elements\UpdatePreconfiguredsContract;
use App\Contracts\Tasks\Elements\UpdateSwitchesContract;
use App\Contracts\Tasks\Taskables\UpdateTaskCheckListsStatusContract;
use App\Contracts\Tasks\Taskables\UpdateTaskFormulaStatusContract;
use App\Contracts\Tasks\Taskables\UpdateTaskSwitchesStatusContract;
use App\Contracts\UpdateTaskStatusContract;
use App\Contracts\UpdateUsersTreeContract;
use App\Contracts\HandlePrevPeriodContract;
use App\Contracts\IndexTaskContract;
use App\Contracts\StoreTaskContract;
use App\Contracts\StoreTaskElementsContract;
use App\Contracts\UpdateTaskContract;
use App\Contracts\UpdateTaskElementsContract;
use App\Contracts\WorkingPeriodContract;
use Illuminate\Support\ServiceProvider;

class ContractServiceProvider extends ServiceProvider
{
    public array $bindings = [
        IndexTaskContract::class => IndexTask::class,
        StoreTaskContract::class => StoreTask::class,
        UpdateTaskContract::class => UpdateTask::class,
        UpdateTaskStatusContract::class => UpdateTaskStatus::class,
        StoreTaskElementsContract::class => StoreTaskElements::class,
        UpdateTaskElementsContract::class => UpdateTaskElements::class,
        StoreTaskCommentContract::class => StoreTaskComment::class,
        StorePeriodContract::class => StorePeriodAction::class,
        ApprovePeriodContract::class => ApprovePeriodAction::class,
        WorkingPeriodContract::class => WorkingPeriodAction::class,
        PlanningPeriodContract::class => PlanningPeriodAction::class,
        ApprovedPeriodContract::class => ApprovedPeriodAction::class,
        HandlePrevPeriodContract::class => HandlePrevPeriod::class,
        UpdateUsersTreeContract::class => UpdateUsersTree::class,

        /*
         * New tasks store
         */
        StoreSwitchesContract::class => StoreSwitches::class,
        StoreCheckListsContract::class => StoreCheckLists::class,
        StoreFormulasContract::class => StoreFormulas::class,
        StorePreconfiguredsContract::class => StorePreconfigureds::class,

        /*
         * New tasks types update
         */
        UpdateSwitchesContract::class => UpdateSwitches::class,
        UpdateCheckListsContract::class => UpdateCheckLists::class,
        UpdateFormulasContract::class => UpdateFormulas::class,
        UpdatePreconfiguredsContract::class => UpdatePreconfigureds::class,

        /*
         * Update taskable statuses
         */
        UpdateTaskSwitchesStatusContract::class => UpdateTaskSwitchesStatus::class,
        UpdateTaskCheckListsStatusContract::class => UpdateTaskCheckListsStatus::class,
        UpdateTaskFormulaStatusContract::class => UpdateTaskFormulaStatus::class
    ];
}
