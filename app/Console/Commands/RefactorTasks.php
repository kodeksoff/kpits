<?php

namespace App\Console\Commands;

use App\Enums\TaskElementsType;
use App\Models\Task;
use Illuminate\Console\Command;

class RefactorTasks extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tasks:refactor';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $tasks = Task::with('elements')->get()->toArray();

        foreach ($tasks as $task) {
            $task_elements = [];
            foreach ($task['elements'] as $element) {
                $task_elements[] = $element;
            }

            foreach ($task_elements as $key => $element) {
                if ($element['type'] === TaskElementsType::radio->value && $key === array_key_first($task['elements']) && $element['price'] !== 0) {
                    array_unshift($task_elements, [
                        'task_id' => $task['id'],
                        'title' => 'Нет',
                        'completed' => 0,
                        'price' => 0,
                        'type' => TaskElementsType::radio,
                        'calculated' => true,
                        'complete_execution' => false
                    ]);
                }
            }

            foreach ($task_elements as $key => $task_element) {
                $task_elements[$key]['complete_execution'] = true;
                if ($task_element['type'] === TaskElementsType::radio->value && $key === array_key_last($task['elements'])) {
                    $task_elements[$key]['complete_execution'] = true;
                }

                if ($task_element['type'] === TaskElementsType::checkbox->value) {
                    $task_elements[$key]['complete_execution'] = true;
                }

                $task_elements[$key]['calculated'] = true;
                $task_elements[$key]['order'] = $key + 1;
            }


            $update_task = Task::find($task['id']);
            $update_task->elements()->delete();
            $update_task->elements()->createMany($task_elements);

            \Log::info('task', $task_elements);
        }
    }
}
