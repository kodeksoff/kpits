<?php

namespace App\Console\Commands;

use App\Enums\TaskElementsType;
use App\Models\Task;
use App\Models\Tasks\TaskCheckList;
use App\Models\Tasks\TaskSwitch;
use App\Models\Tasks\TaskType;
use App\Models\TasksElements\ElementCheckList;
use App\Models\TasksElements\ElementSwitch;
use App\Models\TasksElements\ElementType;
use Illuminate\Console\Command;

class RefactorToNewTasksArchitecture extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tasks:new';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        $tasks = Task::with(['elements', 'userReportingPeriod'])->get();

        foreach ($tasks as $task) {
            if ($task->elements->first()->type === TaskElementsType::radio) {
                $taskable = TaskType::withoutEvents(function () use ($task) {
                    return TaskSwitch::create([
                        'title' => $task->title,
                        'description' => $task->description,
                        'type' => $task->type,
                        'status' => $task->status
                    ]);
                });

                foreach ($task->elements as $element) {
                    $element_switches = ElementType::withoutEvents(function () use ($element, $taskable) {
                        return ElementSwitch::create([
                            'task_switch_id' => $taskable->id,
                            'title' => $element->title,
                            'completed' => $element->completed,
                            'price' => $element->price,
                            'order' => $element->order
                        ]);
                    });

                    $element_switches->payment()->create([
                        'user_reporting_period_id' => $task->user_reporting_period_id,
                        'price' => $element->price,
                        'counting' => $element->completed ? 1 : 0
                    ]);
                }

                $task->taskable()->associate($taskable)->save();
            }

            if ($task->elements->first()->type === TaskElementsType::checkbox) {
                $taskable = TaskType::withoutEvents(function () use ($task) {
                    return TaskCheckList::create([
                        'title' => $task->title,
                        'description' => $task->description,
                        'type' => $task->type,
                        'status' => $task->status
                    ]);
                });

                foreach ($task->elements as $element) {
                    $element_check_lists = ElementType::withoutEvents(function () use ($element, $taskable) {
                        return ElementCheckList::create([
                            'task_check_list_id' => $taskable->id,
                            'title' => $element->title,
                            'completed' => $element->completed,
                            'price' => $element->price,
                            'order' => $element->order
                        ]);
                    });

                    $element_check_lists->payment()->create([
                        'user_reporting_period_id' => $task->user_reporting_period_id,
                        'price' => $element->price,
                        'counting' => $element->completed ? 1 : 0
                    ]);
                }

                $task->taskable()->associate($taskable)->save();
            }
        }
    }
}
