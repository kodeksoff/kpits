<?php

namespace App\Console;

use App\Jobs\CreateReportingPeriod;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly();

        /*
         * Создаем новый отчетный период
         * в первый день месяца
         */
        $schedule->job(new CreateReportingPeriod())->monthly();

        /*
         * Очищаем телескоп за последние 7 дней
         */
        $schedule->command('telescope:prune --hours=168')->weekly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
