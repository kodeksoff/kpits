<?php

namespace App\Models;

use App\Enums\PeriodStatus;
use App\Enums\UserPeriodStatus;
use App\Traits\ObservableCache;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\HigherOrderWhenProxy;

/**
 * @mixin IdeHelperReportingPeriod
 */
class ReportingPeriod extends Model
{
    use HasFactory;
    use ObservableCache;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'title',
        'start_period',
        'end_period',
        'status'
    ];

    /**
     * @var string[]
     */
    protected $casts = [
        'status' => PeriodStatus::class
    ];

    /**
     * @var string[]
     */
    protected $appends = ['current_date', 'days_left', 'status_decoding'];

    /**
     * @return HasMany
     */
    public function userPeriods(): HasMany
    {
        return $this->hasMany(UserReportingPeriod::class, 'reporting_period_id', 'id');
    }

    /**
     * @return HasOne
     */
    public function userPeriod(): HasOne
    {
        return $this->hasOne(UserReportingPeriod::class, 'reporting_period_id', 'id');
    }

    /**
     * @return HasManyThrough
     */
    public function payment(): HasManyThrough
    {
        return $this->hasManyThrough(
            ElementPayment::class,
            UserReportingPeriod::class
        );
    }

    /**
     * @return HasManyThrough
     */
    public function tasks(): HasManyThrough
    {
        return $this->hasManyThrough(
            Task::class,
            UserReportingPeriod::class,
            'reporting_period_id',
            'user_reporting_period_id'
        );
    }

    /**
     * @return Attribute
     */
    public function daysLeft(): Attribute
    {
        return Attribute::make(
            get: fn () => now()->diffInDays(Carbon::create($this->end_period))
        );
    }

    /**
     * @return Attribute
     */
    public function currentDate(): Attribute
    {
        return Attribute::make(
            get: function () {
                $date = now()->translatedFormat('l, d F Y');

                return mb_strtoupper(mb_substr($date, 0, 1, 'UTF-8'), 'UTF-8') .
                    mb_substr($date, 1, mb_strlen($date), 'UTF-8');
            }
        );
    }

    /**
     * @return Attribute
     */
    public function statusDecoding(): Attribute
    {
        return Attribute::make(
            get: fn () => $this->status->details()
        );
    }

    /**
     * @param Builder $builder
     * @param int|null $reporting_period
     * @param int|null $user_id
     * @param bool $in_work
     * @return Builder|HigherOrderWhenProxy|mixed
     */
    public function scopeWithUserTasks(
        Builder $builder,
        int     $reporting_period = null,
        int     $user_id = null,
        bool    $in_work = true
    ): mixed {
        return $builder->with(['userPeriod' => function ($query) use ($in_work, $user_id) {
            $query->withPayments()->withTasksInWork($user_id ?? auth()->id(), $in_work);
        }])
            ->when($reporting_period, function ($query) use ($reporting_period) {
                $query->where('id', $reporting_period);
            })->when(!$reporting_period, function ($query) use ($reporting_period) {
                $query->where('id', ReportingPeriod::currentPeriod()->id);
            });
    }

    /**
     * @param Builder $builder
     * @return Builder
     */
    public function scopeActualPeriod(Builder $builder): Builder
    {
        return $builder->where(function ($query) {
            $query->where('status', PeriodStatus::in_work);
        });
    }

    /**
     * @param Builder $builder
     * @return Builder
     */
    public function scopeWithUserPeriodCount(Builder $builder): Builder
    {
        return $builder
            ->withCount(['userPeriod as user_periods_total'])
            ->withCount(['userPeriod as user_periods_approved' => fn ($q) => $q->where('status', UserPeriodStatus::approved)]);
    }

    /**
     * @param Builder $builder
     * @return Builder
     */
    public function scopeWithPayments(Builder $builder): Builder
    {
        return $builder->withSum(
            ['payment as payment' => fn ($q) => $q->whereHas(
                'userReportingPeriod',
                fn ($q) => $q->where('status', UserPeriodStatus::approved)->whereNot('user_id', 1)
            )
                ->where('counting', 1)
            ],
            'price'
        );
    }

    /**
     * @param int $id
     * @return mixed
     */
    public static function getWithUserPeriodCount(int $id): mixed
    {
        return Cache::tags(['tasks', 'task_elements', 'task_comments', 'reporting_periods', 'user_reporting_periods'])
            ->remember('reporting_periods_with_count_' . $id, 360 * 60, function () use ($id) {
                return self::withUserPeriodCount()->withPayments()->find($id);
            });
    }

    /**
     * @return mixed
     */
    public static function currentPeriod(): mixed
    {
        return Cache::tags(['tasks', 'task_elements', 'task_comments', 'reporting_periods', 'user_reporting_periods'])
            ->remember('reporting_periods_current_' . now()->startOfMonth(), 360 * 60, function () {
                return self::actualPeriod()->first();
            });
    }

    /**
     * @return mixed
     */
    public static function availablePeriods(): mixed
    {
        return Cache::tags(['tasks', 'task_elements', 'task_comments', 'reporting_periods', 'user_reporting_periods'])
            ->remember('reporting_periods_available_' . now()->year, 360 * 60, function () {
                return self::where(function ($query) {
                    $query->where('start_period', 'like', now()->year . '%')
                        ->where('end_period', '<=', now()->endOfMonth());
                })->get();
            });
    }

    /**
     * @return mixed
     */
    public static function forCurrentYear(): mixed
    {
        return Cache::tags(['tasks', 'task_elements', 'task_comments', 'reporting_periods', 'user_reporting_periods'])
            ->remember('reporting_periods_' . now()->year, 360 * 60, function () {
                return self::where('start_period', 'like', now()->year . '%')->get();
            });
    }

    /**
     * @return mixed
     */
    public static function periodStatistic(): mixed
    {
        return Cache::tags(['tasks', 'task_elements', 'task_comments', 'reporting_periods', 'user_reporting_periods'])
            ->remember('reporting_periods_current_statistic_' . now()->startOfMonth(), 360 * 60, function () {
                return self::withCount([
                    'tasks as tasks_count',
                    'tasks as tasks_completed' =>
                        fn ($q) => $q->whereHasMorph(
                            'taskable',
                            '*',
                            fn ($q) => $q->where('status', 'completed')
                        ),
                    'userPeriods as period_count',
                    'userPeriods as period_completed' =>
                        fn ($q) => $q->where('user_reporting_periods.status', UserPeriodStatus::approved)
                ])
                    ->actualPeriod()
                    ->first();
            });
    }

    /**
     * @param int $period_id
     * @return mixed
     */
    public static function toPay(int $period_id): mixed
    {
        return Cache::tags(['tasks', 'task_elements', 'task_comments', 'reporting_periods', 'user_reporting_periods'])
            ->remember('reporting_periods_to_pay_' . $period_id, 360 * 60, function () use ($period_id) {
                return self::withSum(['userPeriods as payment' => fn ($q) => $q->whereNot('user_id', 1)], 'payment')
                    ->where('reporting_periods.id', $period_id)
                    ->first();
            });
    }
}
