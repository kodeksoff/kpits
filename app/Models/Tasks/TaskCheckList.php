<?php

namespace App\Models\Tasks;

use App\Models\TasksElements\ElementCheckList;
use App\Models\UserReportingPeriod;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @mixin IdeHelperTaskCheckList
 */
class TaskCheckList extends TaskType
{
    use HasFactory;

    protected $fillable = [
        'title',
        'description',
        'type',
        'status'
    ];

    /**
     * @return HasMany
     */
    public function elements(): HasMany
    {
        return $this->hasMany(ElementCheckList::class, 'task_check_list_id', 'id');
    }


    public static function createNew(array $input): void
    {
        $task_type = TaskCheckList::create(['title' => $input['title']]);

        $user_period_id = UserReportingPeriod::where('user_id', $input['user_id'])
            ->where('reporting_period_id', $input['period_id'])
            ->first()
            ->id;

        $task_type->task()->create(
            [
                'user_id' => $input['user_id'],
                'user_reporting_period_id' => $user_period_id
            ]
        );
    }

    /**
     * @param Builder $bulder
     * @return Builder
     */
    public function scopeWithPayments(Builder $bulder): Builder
    {
        return $bulder->withSum('elements', 'price');
    }
}
