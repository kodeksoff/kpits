<?php

namespace App\Models\Tasks;

use App\Enums\TaskTypes;
use App\Models\Task;
use App\Traits\ObservableCache;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Database\Eloquent\Relations\Relation;

abstract class TaskType extends Model
{
    use HasFactory;
    use ObservableCache;

    protected $fillable = [
        'title',
        'description',
        'type',
        'status'
    ];

    /**
     * @var string[]
     */
    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    /**
     * @var string[]
     */
    protected $casts = [
        'type' => TaskTypes::class
    ];

    /**
     * @var string[]
     */
    protected $appends = ['type_decoding'];

    /**
     * @return MorphOne
     */
    public function task(): MorphOne
    {
        return $this->morphOne(Task::class, 'taskable');
    }

    /**
     * @return Relation
     */
    abstract public function elements(): Relation;


    /**
     * @return Attribute
     */
    public function typeDecoding(): Attribute
    {
        return Attribute::make(
            get: fn () => $this->type?->details()
        );
    }

    /**
     * @param array $input
     * @return void
     */
    abstract public static function createNew(array $input): void;

    /**
     * @param Builder $bulder
     * @return Builder
     */
    abstract public function scopeWithPayments(Builder $bulder): Builder;
}
