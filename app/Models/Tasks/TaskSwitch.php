<?php

namespace App\Models\Tasks;

use App\Models\ElementPayment;
use App\Models\TasksElements\ElementSwitch;
use App\Models\UserReportingPeriod;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @mixin IdeHelperTaskSwitch
 */
class TaskSwitch extends TaskType
{
    use HasFactory;

    /**
     * @var string[]
     */
    protected $fillable = [
        'title',
        'description',
        'type',
        'status'
    ];

    /**
     * @return HasMany
     */
    public function elements(): HasMany
    {
        return $this->hasMany(ElementSwitch::class, 'task_switch_id', 'id');
    }

    public static function createNew(array $input): void
    {
        $task_type = self::create(['title' => $input['title']]);

        $user_period_id = UserReportingPeriod::where('user_id', $input['user_id'])
            ->where('reporting_period_id', $input['period_id'])
            ->first()
            ->id;

        $task_type->task()->create(
            [
                'user_id' => $input['user_id'],
                'user_reporting_period_id' => $user_period_id
            ]
        );

        $element = $task_type->elements()->create([
            'title' => 'Нет',
            'completed' => 1,
            'price' => 0,
            'order' => 1
        ]);


        $payment = new ElementPayment();
        $payment->elementable()->associate($element);
        $payment->user_reporting_period_id = $user_period_id;
        $payment->price = 0;
        $payment->save();
    }

    /**
     * @param Builder $bulder
     * @return Builder
     */
    public function scopeWithPayments(Builder $bulder): Builder
    {
        return $bulder->withSum('elements', 'price');
    }
}
