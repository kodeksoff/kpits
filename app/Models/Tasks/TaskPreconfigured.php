<?php

namespace App\Models\Tasks;

use App\Models\ElementPayment;
use App\Models\TasksElements\ElementPreconfigured;
use App\Models\UserReportingPeriod;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\Relation;

/**
 * @mixin IdeHelperTaskPreconfigured
 */
class TaskPreconfigured extends TaskType
{
    use HasFactory;

    public function elements(): Relation
    {
        return $this->hasMany(ElementPreconfigured::class, 'task_preconfigured_id', 'id');
    }

    public static function createNew(array $input): void
    {
        $task_type = self::create(['title' => $input['title']]);

        $user_period_id = UserReportingPeriod::where('user_id', $input['user_id'])
            ->where('reporting_period_id', $input['period_id'])
            ->first()
            ->id;

        $task_type->task()->create(
            [
                'user_id' => $input['user_id'],
                'user_reporting_period_id' => $user_period_id
            ]
        );

        $element = $task_type->elements()->create([
            'price' => 0,
            'completed' => 0,
            'type' => 'completed_sdek_contracts'
        ]);

        $payment = new ElementPayment();
        $payment->elementable()->associate($element);
        $payment->user_reporting_period_id = $user_period_id;
        $payment->price = 0;
        $payment->save();
    }

    public function scopeWithPayments(Builder $bulder): Builder
    {
        // TODO: Implement scopeWithPayments() method.
    }
}
