<?php

namespace App\Models\Tasks;

use App\Models\ElementPayment;
use App\Models\TasksElements\ElementFormula;
use App\Models\UserReportingPeriod;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\Relation;

/**
 * @mixin IdeHelperTaskFormula
 */
class TaskFormula extends TaskType
{
    use HasFactory;

    protected $fillable = [
        'title',
        'description',
        'type',
        'status'
    ];

    public function elements(): Relation
    {
        return $this->hasMany(ElementFormula::class, 'task_formula_id', 'id');
    }

    public static function createNew(array $input): void
    {
        $task_type = self::create(['title' => $input['title']]);

        $user_period_id = UserReportingPeriod::where('user_id', $input['user_id'])
            ->where('reporting_period_id', $input['period_id'])
            ->first()
            ->id;

        $task_type->task()->create(
            [
                'user_id' => $input['user_id'],
                'user_reporting_period_id' => $user_period_id
            ]
        );

        $element = $task_type->elements()->create([
            'formula' => null,
            'price' => 0,
            'completed' => 0,
        ]);


        $payment = new ElementPayment();
        $payment->elementable()->associate($element);
        $payment->user_reporting_period_id = $user_period_id;
        $payment->price = 0;
        $payment->save();
    }

    public function scopeWithPayments(Builder $bulder): Builder
    {
        // TODO: Implement scopeWithPayments() method.
    }
}
