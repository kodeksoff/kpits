<?php

namespace App\Models;

use App\Enums\TaskElementsType;
use App\Traits\ObservableCache;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @mixin IdeHelperTaskElement
 */
class TaskElement extends Model
{
    use HasFactory;
    use ObservableCache;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'title',
        'description',
        'price',
        'type',
        'calculated',
        'completed',
        'complete_execution',
        'order',
    ];

    /**
     * @var string[]
     */
    protected $casts = [
        'type' => TaskElementsType::class
    ];

    /**
     * @return BelongsTo
     */
    public function task(): BelongsTo
    {
        return $this->belongsTo(Task::class, 'task_id', 'id');
    }
}
