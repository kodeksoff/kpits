<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphTo;

/**
 * @mixin IdeHelperElementPayment
 */
class ElementPayment extends Model
{
    use HasFactory;

    /**
     * @var string[]
     */
    protected $fillable = [
        'elementable_type',
        'elementable_id',
        'user_reporting_period_id',
        'price',
        'counting'
    ];

    /**
     * @var string[]
     */
    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    /**
     * @return MorphTo
     */
    public function elementable(): MorphTo
    {
        return $this->morphTo();
    }

    /**
     * @return BelongsTo
     */
    public function userReportingPeriod(): BelongsTo
    {
        return $this->belongsTo(UserReportingPeriod::class, 'user_reporting_period_id', 'id');
    }
}
