<?php

namespace App\Models;

use App\Enums\UserPeriodStatus;
use App\Enums\TaskTypes;
use App\Models\Tasks\TaskCheckList;
use App\Models\Tasks\TaskFormula;
use App\Models\Tasks\TaskPreconfigured;
use App\Models\Tasks\TaskSwitch;
use App\Traits\ObservableCache;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

/**
 * @mixin IdeHelperTask
 */
class Task extends Model
{
    use HasFactory;
    use ObservableCache;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'taskable_type',
        'taskable_id',
        'user_id',
        'user_reporting_period_id',
        'order',
    ];

    /**
     * @var string[]
     */
    protected $casts = [
        'type' => TaskTypes::class
    ];

    /**
     * @var string[]
     */
    protected $appends = ['type_decoding'];

    /**
     * @return MorphTo
     */
    public function taskable(): MorphTo
    {
        return $this->morphTo();
    }

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * @return BelongsTo
     */
    public function userReportingPeriod(): BelongsTo
    {
        return $this->belongsTo(UserReportingPeriod::class, 'user_reporting_period_id', 'id');
    }

    /**
     * @return HasMany
     * TODO REMOVE
     */
    public function elements(): HasMany
    {
        return $this->hasMany(TaskElement::class, 'task_id', 'id');
    }

    /**
     * @return HasOne
     */
    public function comment(): HasOne
    {
        return $this->hasOne(TaskComment::class, 'task_id', 'id');
    }

    /**
     * @return Attribute
     */
    public function typeDecoding(): Attribute
    {
        return Attribute::make(
            get: fn () => $this->type->details()
        );
    }

    /**
     * @param Builder $builder
     * @return Builder
     */
    public function scopeWithPayments(Builder $builder): Builder
    {
        return $builder->withSum(
            ['elements as payment_total' =>
                fn ($q) => $q->select(DB::raw('coalesce(sum(price), 0)'))
                    ->where('calculated', 1)
                    ->where('complete_execution', 1)
            ],
            'price'
        )
            ->withSum(
                ['elements as payment' =>
                    fn ($q) => $q->select(DB::raw('coalesce(sum(price), 0)'))
                        ->where(
                            fn ($q) => $q->where('calculated', 1)->where('completed', 1)
                        )],
                'price'
            );
    }

    /**
     * @param Builder $builder
     * @param $request
     * @return Builder
     */
    public function scopeStatusFiltering(Builder $builder, $request): Builder
    {
        return $builder->where(function ($query) use ($request) {
            $query->when(
                $request->regular,
                fn ($q) => $q->whereHasMorph(
                    'taskable',
                    [
                        TaskSwitch::class,
                        TaskCheckList::class,
                        TaskFormula::class,
                        TaskPreconfigured::class
                    ],
                    fn ($q) => $q->where('type', TaskTypes::regular)
                )
            );
            $query->when(
                $request->urgent,
                fn ($q) => $q->whereHasMorph(
                    'taskable',
                    [
                        TaskSwitch::class,
                        TaskCheckList::class,
                        TaskFormula::class,
                        TaskPreconfigured::class
                    ],
                    fn ($q) => $q->where('type', TaskTypes::urgent)
                )
            );
            $query->when(
                $request->long_term,
                fn ($q) => $q->whereHasMorph(
                    'taskable',
                    [
                        TaskSwitch::class,
                        TaskCheckList::class,
                        TaskFormula::class,
                        TaskPreconfigured::class
                    ],
                    fn ($q) => $q->where('type', TaskTypes::long_term)
                )
            );
        });
    }


    /**
     * @param Collection $reporting_periods
     * @param int|null $reporting_period
     * @return mixed
     */
    public static function userTaskList(Collection $reporting_periods, int $reporting_period = null): mixed
    {
        return Cache::tags(['tasks', 'task_elements', 'task_comments', 'reporting_periods', 'user_reporting_periods'])
            ->remember(
                'task_list_user_id_' . auth()->id() . '_for_' . $reporting_period . '_period',
                360 * 60,
                function () use ($reporting_period, $reporting_periods) {
                    return self::with(['elements', 'comment'])
                        ->whereHas('reportingPeriod.userPeriods', function (Builder $builder) {
                            $builder->where('user_id', auth()->id());
                        })
                        ->where('user_id', auth()->id())
                        ->when($reporting_period, function ($query) use ($reporting_period) {
                            $query->where('reporting_period_id', $reporting_period);
                        })->when(!$reporting_period, function ($query) use ($reporting_periods) {
                            $query->where('reporting_period_id', $reporting_periods->last()?->id);
                        })
                        ->get();
                }
            );
    }

    /**
     * @param Request $request
     * @return LengthAwarePaginator
     */
    public static function resultsWithFiltering(Request $request): LengthAwarePaginator
    {
        return self::withPayments()
            ->with([
                'elements' => fn ($q) => $q->orderBy('completed', 'desc'),
                'user',
                'userReportingPeriod'
            ])
            ->where(function ($query) use ($request) {
                $query->when(
                    auth()->user()->permissions['access']['results'] === 'view_subordinates',
                    fn ($q) => $q->orWhereHas(
                        'userReportingPeriod',
                        fn (Builder $b) => $b->whereIn('user_id', auth()->user()->getSubordinatesIds())
                    )
                );
            })
            ->where(function ($query) use ($request) {
                $query->when(
                    $request->reporting_period,
                    fn ($q) => $q->orWhereHas(
                        'userReportingPeriod',
                        fn (Builder $b) => $b->where('reporting_period_id', $request->reporting_period)
                    )
                );
            })
            ->where(function ($query) use ($request) {
                $query->when(
                    $request->status,
                    fn ($q) => $q->orWhereHas(
                        'userReportingPeriod',
                        fn (Builder $b) => $b->where('status', UserPeriodStatus::from($request->status))
                    )
                );
            })
            ->where(function ($query) use ($request) {
                $query->when(
                    $request->user,
                    fn ($q) => $q->orWhereHas(
                        'userReportingPeriod',
                        fn (Builder $b) => $b->where('user_id', $request->user)
                    )
                );
            })
            ->where(function ($query) use ($request) {
                $query->when(
                    $request->with_results,
                    fn ($q) => $q->orWhereHas(
                        'elements',
                        fn (Builder $b) => $b->where('completed', 1)
                    )
                );
                $query->when(
                    $request->no_results,
                    fn ($q) => $q->orWhereDoesntHave(
                        'elements',
                        fn (Builder $b) => $b->where('completed', 1)
                    )
                );
            })
            ->where(function ($query) use ($request) {
                $query->when($request->urgent, fn ($q) => $q->orWhere('type', TaskTypes::urgent));
                $query->when($request->regular, fn ($q) => $q->orWhere('type', TaskTypes::regular));
                $query->when($request->long_term, fn ($q) => $q->orWhere('type', TaskTypes::long_term));
            })
            ->orderBy('user_id')
            ->paginate(45)
            ->withQueryString()
            ->through(fn ($task) => [
                'id' => $task->id,
                'title' => $task->title,
                'user_id' => $task->user_id,
                'user_reporting_period_id' => $task->user_reporting_period_id,
                'type' => $task->type,
                'status' => $task->status,
                'order' => $task->order,
                'payment_total' => $task->payment_total,
                'payment' => $task->payment,
                'type_decoding' => $task->type_decoding,
                'elements' => $task->elements,
                'user' => $task->user,
                'user_reporting_period' => $task->userReportingPeriod
            ]);
    }
}
