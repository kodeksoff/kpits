<?php

namespace App\Models;

use App\Enums\UserPeriodStatus;
use App\Traits\HasProfilePhoto;
use App\Traits\ObservableCache;
use App\Utilities\Helpers\Helper;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Gate;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Laravel\Sanctum\HasApiTokens;

/**
 * @mixin IdeHelperUser
 */
class User extends Authenticatable
{
    use HasApiTokens;
    use HasFactory;
    use HasProfilePhoto;
    use Notifiable;
    use TwoFactorAuthenticatable;
    use ObservableCache;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'first_name',
        'second_name',
        'phone',
        'email',
        'counting_id',
        'supervisor_id',
        'profile_photo_path',
        'disabled',
        'password',
        'role_id',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array<int, string>
     */
    protected $appends = [
        'full_name',
        'text',
        'profile_photo_url',
        'permissions'
    ];

    /**
     * @return BelongsTo
     */
    public function role(): BelongsTo
    {
        return $this->belongsTo(Role::class);
    }

    /**
     * @return HasMany
     */
    public function subordinates(): HasMany
    {
        return $this->hasMany(self::class, 'supervisor_id', 'id');
    }

    /**
     * @return BelongsTo
     */
    public function supervisor(): BelongsTo
    {
        return $this->belongsTo(self::class, 'supervisor_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function children(): HasMany
    {
        return $this->subordinates()->with('children');
    }

    /**
     * @return HasManyThrough
     */
    public function tasks(): HasManyThrough
    {
        return $this->hasManyThrough(
            Task::class,
            UserReportingPeriod::class,
            'user_id',
            'user_reporting_period_id'
        );
    }

    /**
     * @return HasMany
     */
    public function periods(): HasMany
    {
        return $this->hasMany(UserReportingPeriod::class, 'user_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function favorites(): HasMany
    {
        return $this->hasMany(Favorites::class, 'user_id', 'id');
    }

    /**
     * @return BelongsToMany
     */
    public function favoriters(): BelongsToMany
    {
        return $this->belongsToMany(
            self::class,
            'favorites',
            'favoriteable_id',
            'user_id'
        )->where('favoriteable_type', $this->getMorphClass());
    }

    /**
     * @return HasOne
     */
    public function hasFavorite(): HasOne
    {
        return $this->hasOne(Favorites::class, 'favoriteable_id', 'id')
            ->where(function ($query) {
                $query->where('favoriteable_type', $this->getMorphClass());
                $query->where('user_id', auth()->id());
            });
    }

    /**
     * @return HasOne
     */
    public function snakeScore(): HasOne
    {
        return $this->hasOne(SnakeScore::class, 'user_id', 'id');
    }

    /**
     * @return Attribute
     */
    public function fullName(): Attribute
    {
        return Attribute::make(
            get: fn () => "$this->first_name $this->second_name"
        );
    }

    /**
     * @return Attribute
     * НЕ КОСТЫЛЬ А КОСТЫЛИШЕ
     * https://he-tree-vue.phphe.com/
     * ЭТА ДИЧЬ НЕ МОЖЕТ ПОМЕНЯТЬ node.text на node.full_name
     * вписту []
     *
     * Если ты это читаешь, знай, кто то страдал делая это
     */
    public function text(): Attribute
    {
        return Attribute::make(
            get: fn () => "#$this->id - $this->first_name $this->second_name"
        );
    }

    /**
     * @return Attribute
     */
    public function phone(): Attribute
    {
        return Attribute::make(
            set: fn ($value) => preg_replace('/\D+/', '', $value)
        );
    }

    /**
     * @param Builder $builder
     * @return Builder
     */
    public function scopeChiefs(Builder $builder): Builder
    {
        return $builder->whereIn('role_id', [1, 2, 3, 4]);
    }

    /**
     * @param int $id
     * @return Model
     */
    public function addFavorite(int $id): Model
    {
        return $this->favorites()->create([
            'favoriteable_type' => $this->getMorphClass(),
            'favoriteable_id' => $id
        ]);
    }

    /**
     * @param int $id
     * @return int
     */
    public function deleteFavorite(int $id): int
    {
        return $this->favorites()->where(function ($query) use ($id) {
            $query
                ->where('favoriteable_type', $this->getMorphClass())
                ->where('favoriteable_id', $id);
        })->delete();
    }

    /**
     * @return mixed
     */
    public static function getListFromCache(): mixed
    {
        return Cache::tags(['users', 'departments', 'roles'])
            ->remember('users_list', 360 * 60, function () {
                return self::get();
            });
    }

    /**
     * @return mixed
     */
    public static function getSubordinatesFromCache(): mixed
    {
        return Cache::tags(['users', 'departments', 'roles'])
            ->remember('users_subordinates_' . auth()->id(), 360 * 60, function () {
                return self::whereIn('id', auth()->user()->getSubordinatesIds())->get();
            });
    }

    /**
     * @return Attribute
     */
    public function permissions(): Attribute
    {
        return Attribute::make(
            get: fn () => Cache::tags(['users', 'departments', 'roles'])
                ->remember('permissions_user_' . $this->id, 360 * 60, function () {
                    return [
                        'access' => [
                            'admin' => Gate::allows('access-admin'),
                            'results' => $this->load('role')->role->results_view->value ?? false,
                            'manager' => Gate::allows('access-manager'),
                            'accountant' => Gate::allows('access-accountant'),
                        ],
                    ];
                })
        );
    }

    /**
     * @return mixed
     */
    public static function getRecursiveTree(): mixed
    {
        return Cache::tags(['users', 'departments', 'roles'])
            ->remember('users_recursive_depth', 360 * 60, function () {
                return User::with('children')
                    ->whereNull('supervisor_id')
                    ->first();
            });
    }

    /**
     * @return mixed
     */
    public static function getSubordinatesIds(): mixed
    {
        return Cache::tags(['users', 'departments', 'roles'])
            ->remember('user_recursive_ids_' . auth()->id(), 360 * 60, function () {
                $user = auth()->user()->load(['children'])->toArray();
                $items = Helper::parseArrayTree([$user]);

                unset($items[0]);

                $return = [];
                foreach ($items as $item) {
                    $return[] = $item['id'];
                }

                return $return;
            });
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public static function getPeriodWithTaskCount(Request $request): mixed
    {
        return Cache::tags(['tasks', 'task_elements', 'task_comments', 'reporting_periods', 'user_reporting_periods'])
            ->remember(
                'period_with_tasks_count_' . auth()->id() . '_' . implode('_', $request->all()),
                360 * 60,
                function () use ($request) {
                    return self::with([
                        'periods' => function ($query) use ($request) {
                            $query->where('reporting_period_id', $request->reporting_period)
                                ->withPayments()
                                ->withCount('tasks');
                        }])
                        ->withCount(['hasFavorite'])
                        ->when(
                            $request->only_favorites,
                            fn ($q) => $q->whereIn('id', auth()->user()->favorites()->withType(self::class)->pluck('favoriteable_id')),
                            fn ($q) => $q->when(
                                auth()->user()->role_id === 4,
                                fn ($q) => $q->whereNot('id', 1),
                                fn ($q) => $q->whereIn('id', auth()->user()->getSubordinatesIds()),
                            ),
                        )
                        ->where(function ($query) use ($request) {
                            $query->whereHas('periods', function (Builder $builder) use ($request) {
                                $builder->where('reporting_period_id', $request->reporting_period)
                                    ->where(function ($query) use ($request) {
                                        $query->when(
                                            $request->with_tasks,
                                            fn ($q) => $q->orWhereHas('tasks')
                                        );
                                        $query->when(
                                            $request->without_tasks,
                                            fn ($q) => $q->orWhereDoesntHave('tasks')
                                        );
                                    })
                                    ->where(function ($query) use ($request) {
                                        $query->when(
                                            $request->planning,
                                            fn ($q) => $q->orWhere('status', UserPeriodStatus::planning)
                                        );
                                        $query->when(
                                            $request->in_work,
                                            fn ($q) => $q->orWhere('status', UserPeriodStatus::in_work)
                                        );
                                        $query->when(
                                            $request->under_review,
                                            fn ($q) => $q->orWhere('status', UserPeriodStatus::under_review)
                                        );
                                        $query->when(
                                            $request->approved,
                                            fn ($q) => $q->orWhere('status', UserPeriodStatus::approved)
                                        );
                                    });
                            })->when($request->without_period, function (Builder $builder) use ($request) {
                                $builder->orWhereDoesntHave(
                                    'periods',
                                    fn ($q) => $q->where('reporting_period_id', $request->reporting_period)
                                );
                            });
                        })
                        ->when(
                            $request->search,
                            fn ($q) => $q->where(
                                fn ($q) => $q
                                    ->orWhere('first_name', 'like', '%' . $request->search . '%')
                                    ->orWhere('second_name', 'like', '%' . $request->search . '%')
                            )
                        )
                        ->paginate(45)
                        ->withQueryString()
                        ->through(fn ($user) => [
                            'id' => $user->id,
                            'first_name' => $user->first_name,
                            'second_name' => $user->second_name,
                            'full_name' => $user->full_name,
                            'phone' => $user->phone,
                            'email' => $user->email,
                            'role_id' => $user->role_id,
                            'permissions' => $user->permissions,
                            'has_favorite_count' => $user->has_favorite_count,
                            'periods' => $user->periods
                        ]);
                }
            );
    }
}
