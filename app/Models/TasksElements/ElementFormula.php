<?php

namespace App\Models\TasksElements;

use App\Actions\Tasks\Elements\ElementsStore;
use App\Actions\Tasks\Elements\ElementsUpdate;
use App\Contracts\Tasks\Elements\StoreFormulasContract;
use App\Contracts\Tasks\Elements\UpdateFormulasContract;
use App\Models\Tasks\TaskFormula;
use App\Models\Tasks\TaskType;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @mixin IdeHelperElementFormula
 */
class ElementFormula extends ElementType
{
    use HasFactory;

    protected $fillable = [
        'task_formula_id',
        'formula',
        'result',
        'price',
        'completed',
        'order'
    ];

    /**
     * @return BelongsTo
     */
    public function taskElement(): BelongsTo
    {
        return $this->belongsTo(TaskFormula::class, 'task_formula_id', 'id');
    }

    /**
     * @param TaskType $taskable
     * @param array $input
     * @return ElementsStore
     */
    public function handleStore(TaskType $taskable, array $input): ElementsStore
    {
        return (self::$store)($this, $taskable, $input);
    }

    /**
     * @param TaskType $taskable
     * @param array $input
     * @return ElementsUpdate
     */
    public function handleUpdate(TaskType $taskable, array $input): ElementsUpdate
    {
        return (self::$update)($this, $taskable, $input);
    }

    /**
     * @return void
     */
    protected static function boot(): void
    {
        parent::boot();

        self::$update = app(UpdateFormulasContract::class);

        self::$store = app(StoreFormulasContract::class);
    }
}
