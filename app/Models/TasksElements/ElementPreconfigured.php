<?php

namespace App\Models\TasksElements;

use App\Actions\Tasks\Elements\ElementsStore;
use App\Actions\Tasks\Elements\ElementsUpdate;
use App\Contracts\Tasks\Elements\StoreFormulasContract;
use App\Contracts\Tasks\Elements\StorePreconfiguredsContract;
use App\Contracts\Tasks\Elements\UpdateFormulasContract;
use App\Contracts\Tasks\Elements\UpdatePreconfiguredsContract;
use App\Models\Tasks\TaskPreconfigured;
use App\Models\Tasks\TaskType;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ElementPreconfigured extends ElementType
{
    use HasFactory;

    use HasFactory;

    protected $fillable = [
        'task_preconfigured_id',
        'price',
        'completed',
        'order',
        'input',
        'type'
    ];

    protected $casts = [
        'input' => 'array'
    ];

    /**
     * @return BelongsTo
     */
    public function taskElement(): BelongsTo
    {
        return $this->belongsTo(TaskPreconfigured::class, 'task_preconfigured_id', 'id');
    }

    /**
     * @param TaskType $taskable
     * @param array $input
     * @return ElementsStore
     */
    public function handleStore(TaskType $taskable, array $input): ElementsStore
    {
        return (self::$store)($this, $taskable, $input);
    }

    /**
     * @param TaskType $taskable
     * @param array $input
     * @return ElementsUpdate
     */
    public function handleUpdate(TaskType $taskable, array $input): ElementsUpdate
    {
        return (self::$update)($this, $taskable, $input);
    }

    /**
     * @return void
     */
    protected static function boot(): void
    {
        parent::boot();

        self::$update = app(UpdatePreconfiguredsContract::class);

        self::$store = app(StorePreconfiguredsContract::class);
    }
}
