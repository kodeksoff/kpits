<?php

namespace App\Models\TasksElements;

use App\Actions\Tasks\Elements\ElementsStore;
use App\Actions\Tasks\Elements\ElementsUpdate;
use App\Models\ElementPayment;
use App\Models\Tasks\TaskType;
use App\Traits\ObservableCache;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Database\Eloquent\Relations\Relation;

abstract class ElementType extends Model
{
    use HasFactory;
    use ObservableCache;

    /**
     * @var string[]
     */
    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    /**
     * @var ElementsStore
     */
    protected static ElementsStore $store;

    /**
     * @var ElementsUpdate
     */
    protected static ElementsUpdate $update;

    /**
     * @return Relation
     */
    abstract public function taskElement(): Relation;

    /**
     * @return MorphOne
     */
    public function payment(): MorphOne
    {
        return $this->morphOne(ElementPayment::class, 'elementable');
    }

    /**
     * @param TaskType $taskable
     * @param array $input
     * @return ElementsStore
     */
    abstract public function handleStore(TaskType $taskable, array $input): ElementsStore;

    /**
     * @param TaskType $taskable
     * @param array $input
     * @return ElementsUpdate
     */
    abstract public function handleUpdate(TaskType $taskable, array $input): ElementsUpdate;
}
