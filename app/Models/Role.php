<?php

namespace App\Models;

use App\Enums\Roles\ResultViewType;
use App\Traits\ObservableCache;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @mixin IdeHelperRole
 */
class Role extends Model
{
    use HasFactory;
    use ObservableCache;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'title',
        'is_admin',
    ];

    /**
     * @var string[]
     */
    protected $casts = [
        'results_view' => ResultViewType::class
    ];
}
