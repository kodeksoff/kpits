<?php

namespace App\Models;

use App\Enums\UserPeriodStatus;
use App\Traits\ObservableCache;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\HigherOrderWhenProxy;

/**
 * @mixin IdeHelperUserReportingPeriod
 */
class UserReportingPeriod extends Model
{
    use HasFactory;
    use ObservableCache;

    /**
     * @var string[]
     */
    protected $fillable = [
        'user_id',
        'reporting_period_id',
        'status'
    ];

    /**
     * @var string[]
     */
    protected $casts = [
        'status' => UserPeriodStatus::class
    ];

    /**
     * @var string[]
     */
    protected $appends = ['status_decoding'];

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * @return BelongsTo
     */
    public function reportingPeriod(): BelongsTo
    {
        return $this->belongsTo(ReportingPeriod::class, 'reporting_period_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function tasks(): HasMany
    {
        return $this->hasMany(Task::class, 'user_reporting_period_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function payments(): HasMany
    {
        return $this->hasMany(ElementPayment::class, 'user_reporting_period_id', 'id');
    }

    /**
     * @return HasManyThrough
     */
    public function elements(): HasManyThrough
    {
        return $this->hasManyThrough(
            TaskElement::class,
            Task::class
        );
    }

    /**
     * @return Attribute
     */
    public function statusDecoding(): Attribute
    {
        return Attribute::make(
            get: fn () => $this->status->details()
        );
    }

    /**
     * @param Builder $builder
     * @return Builder
     */
    public function scopeWithPayments(Builder $builder): Builder
    {
        return $builder->withSum(
            ['payments as payment' => fn ($q) => $q->where('counting', 1)
                ->whereNot('user_id', 1)
            ],
            'price'
        );
    }

    /**
     * @return mixed
     */
    public static function statistic(): mixed
    {
        return Cache::tags(['tasks', 'task_elements', 'task_comments', 'reporting_periods', 'user_reporting_periods'])
            ->remember(
                'user_tasks_statistic_' . auth()->id() . '_' . ReportingPeriod::currentPeriod()->id,
                360 * 60,
                function () {
                    return self::withCount([
                        'tasks as tasks_count',
                        'tasks as tasks_completed' =>
                            fn ($q) => $q->whereHasMorph(
                                'taskable',
                                '*',
                                fn ($q) => $q->where('status', 'completed')
                            )
                    ])
                        ->withPayments()
                        ->where(function ($query) {
                            $query->where('user_id', auth()->id())
                                ->where('reporting_period_id', ReportingPeriod::currentPeriod()->id)
                                ->inWork();
                        })
                        ->first();
                }
            );
    }


    /**
     * @param int $period_id
     * @return Collection|array
     */
    public static function resultsWithPayment(int $period_id): Collection|array
    {
        return self::with(['user'])
            ->withPayments()
            ->whereNot('user_id', 1)
            ->where('reporting_period_id', $period_id)
            ->get();
    }

    /**
     * @param Request $request
     * @return LengthAwarePaginator
     */
    public static function resultsWithFiltering(Request $request): LengthAwarePaginator
    {
        return self::with(['user' => fn ($q) => $q->withCount(['hasFavorite'])])
            ->withCount(['tasks as tasks_count' => function ($query) use ($request) {
                $query->statusFiltering($request);
            }])
            ->withCount(['tasks as tasks_completed' => function ($query) use ($request) {
                $query->whereHasMorph(
                    'taskable',
                    '*',
                    fn ($q) => $q->where('status', 'completed')
                    ->orWhere('status', 'partially-completed')
                )
                    ->statusFiltering($request);
            }])
            ->when(
                $request->reporting_period,
                fn ($q) => $q->where('reporting_period_id', $request->reporting_period),
                fn ($q) => $q->where('reporting_period_id', ReportingPeriod::currentPeriod()->id),
            )
            ->when(
                $request->user,
                fn ($q) => $q->where('user_id', $request->user),
                fn ($q) => $q->when(
                    auth()->user()->role_id === 4,
                    fn ($q) => $q->whereNot('id', 1),
                    fn ($q) => $q->whereIn('user_id', auth()->user()->getSubordinatesIds()),
                ),
            )
            ->when(
                $request->only_favorites,
                fn ($q) => $q->whereIn('user_id', auth()->user()->favorites()->withType(User::class)->pluck('favoriteable_id')),
            )
            ->when(
                $request->status,
                fn ($q) => $q->where('status', UserPeriodStatus::from($request->status)),
            )
            ->where(function ($query) use ($request) {
                $query->when(
                    $request->with_results,
                    fn ($q) => $q->orWhereHas(
                        'tasks',
                        fn ($q) => $q->whereHasMorph(
                            'taskable',
                            '*',
                            fn ($q) => $q->where('status', 'completed')
                        )
                    ),
                );
                $query->when(
                    $request->no_results,
                    fn ($q) => $q->orWhereDoesntHave(
                        'tasks',
                        fn ($q) => $q->whereHasMorph(
                            'taskable',
                            '*',
                            fn ($q) => $q->where('status', 'not-completed')
                        )
                    ),
                );
            })
            ->withPayments()
            ->paginate(45)
            ->withQueryString()
            ->through(fn ($period) => [
                'id' => $period->id,
                'user_id' => $period->user_id,
                'reporting_period_id' => $period->reporting_period_id,
                'status' => $period->status,
                'tasks_count' => $period->tasks_count,
                'tasks_completed' => $period->tasks_completed,
                'payment_total' => $period->payment_total,
                'payment' => $period->payment,
                'status_decoding' => $period->status_decoding,
                'user' => $period->user
            ]);
    }

    /**
     * @param Builder $builder
     * @return Builder
     */
    public function scopeInWork(Builder $builder): Builder
    {
        return $builder->whereIn('status', [UserPeriodStatus::in_work, UserPeriodStatus::under_review, UserPeriodStatus::approved]);
    }

    /**
     * @param Builder $builder
     * @param int $user_id
     * @param bool $in_work
     * @return Builder|HigherOrderWhenProxy|mixed
     */
    public function scopeWithTasksInWork(Builder $builder, int $user_id, bool $in_work): mixed
    {
        return $builder->with(['tasks' => fn ($q) => $q->with(['taskable.elements.payment', 'comment'])])
            ->when($in_work, fn ($query) => $query->inWork())
            ->where('user_reporting_periods.user_id', $user_id);
    }

    /**
     * @param int $user_id
     * @return ?Model
     */
    public static function prevPeriod(int $user_id): ?Model
    {
        return self::with(['tasks.elements'])->where('user_id', $user_id)
            ->orderBy('id', 'desc')
            ->skip(1)
            ->first();
    }
}
