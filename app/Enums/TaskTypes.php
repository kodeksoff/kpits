<?php

namespace App\Enums;

enum TaskTypes: string
{
    case regular = 'regular';
    case urgent = 'urgent';
    case long_term = 'long-term';

    public function details(): array
    {
        return match ($this) {
            self::regular => [
                'value' => 'regular',
                'description' => 'Регулярная',
            ],
            self::urgent => [
                'value' => 'urgent',
                'description' => 'Срочная'
            ],
            self::long_term => [
                'value' => 'long_term',
                'description' => 'Долгосрочная'
            ]
        };
    }
}
