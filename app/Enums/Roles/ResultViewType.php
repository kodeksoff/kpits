<?php

namespace App\Enums\Roles;

enum ResultViewType: string
{
    case view = 'view';
    case view_subordinates = 'view_subordinates';
    case view_only = 'view_only';
    case false = 'false';
}
