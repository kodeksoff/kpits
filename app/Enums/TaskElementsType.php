<?php

namespace App\Enums;

enum TaskElementsType: string
{
    case checkbox = 'checkbox';
    case radio = 'radio';
}
