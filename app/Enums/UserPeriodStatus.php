<?php

namespace App\Enums;

enum UserPeriodStatus: string
{
    case planning = 'planning';
    case in_work = 'in_work';
    case under_review = 'under_review';
    case approved = 'approved';

    public function details(): array
    {
        return match ($this) {
            self::planning => [
                'value' => 'planning',
                'description' => 'Планируется',
            ],
            self::in_work => [
                'value' => 'in_work',
                'description' => 'В работе'
            ],
            self::under_review => [
                'value' => 'under_review',
                'description' => 'На проверке'
            ],
            self::approved => [
                'value' => 'approved',
                'description' => 'Утвержден'
            ]
        };
    }
}
