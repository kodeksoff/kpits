<?php

namespace App\Enums;

enum PeriodStatus: string
{
    case in_work = 'in_work';
    case in_payment = 'in_payment';
    case paid = 'paid';

    public function details(): array
    {
        return match ($this) {
            self::in_work => [
                'value' => 'in_work',
                'description' => 'В работе'
            ],
            self::in_payment => [
                'value' => 'in_payment',
                'description' => 'Передан на оплату'
            ],
            self::paid => [
                'value' => 'paid',
                'description' => 'Оплачен'
            ]
        };
    }
}
