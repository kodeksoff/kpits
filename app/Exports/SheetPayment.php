<?php

namespace App\Exports;

use App\Models\UserReportingPeriod;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class SheetPayment implements FromView
{
    public function __construct(private readonly int $period_id)
    {
    }

    public function view(): View
    {
        $periods = UserReportingPeriod::resultsWithPayment($this->period_id);

        return view('Export.SheetPayment', ['periods' => $periods]);
    }
}
