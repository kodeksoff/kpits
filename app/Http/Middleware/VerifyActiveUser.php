<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class VerifyActiveUser
{
    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param  Closure  $next
     * @return Response|RedirectResponse|null
     */
    public function handle(Request $request, Closure $next)
    {
        if ($request->user()->disabled) {
            return abort(403, 'Пользовтаель заблокирован');
        }

        return $next($request);
    }
}
