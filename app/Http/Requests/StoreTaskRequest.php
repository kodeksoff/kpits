<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreTaskRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'task' => 'required|array',
            'task.title' => 'required|string',
            'task.description' => 'nullable|string',
            'task.order' => 'required|integer',
            'task.user_reporting_period_id' => 'required|integer',
            'task.type' => 'string',
            'task.user_id' => 'required|integer',
            'task.elements' => 'required|array',
            'task.elements.*.title' => 'required|string',
            'task.elements.*.type' => 'required|string|in:checkbox,radio',
            'task.elements.*.price' => 'required|integer',
            'task.elements.*.completed' => 'boolean',
            'task.elements.*.task_id' => 'integer',
            'task.elements.*.order' => 'integer',
            'comment' => 'nullable|string',
        ];
    }
}
