<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateTaskRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            'task' => 'required|array',
            'task.title' => 'required|string',
            'task.description' => 'nullable|string',
            'task.user_reporting_period_id' => 'required|integer',
            'task.type' => 'required|string',
            'task.user_id' => 'required|integer',
            'task.elements' => 'required|array',
            'task.elements.*.id' => 'integer',
            'task.elements.*.title' => 'nullable|string',
            'task.elements.*.price' => 'nullable|integer',
            'task.elements.*.formula' => 'nullable|string',
            'task.elements.*.completed' => 'nullable|boolean',
            'task.elements.*.order' => 'nullable|integer',
            'task.elements.*.type' => 'nullable|string',
            'task.elements.*.input' => 'nullable|array',
            'comment' => 'nullable|string',
        ];
    }
}
