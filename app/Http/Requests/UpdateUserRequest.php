<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Laravel\Fortify\Rules\Password;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'first_name' => ['required', 'string', 'max:255'],
            'second_name' => ['string', 'max:255'],
            'phone' => ['required', 'string', 'max:255', Rule::unique('users')->ignore($this->user->id)],
            'email' => ['nullable', 'string', 'max:255', Rule::unique('users')->ignore($this->user->id)],
            'role_id' => ['required', 'exists:roles,id'],
            'counting_id' => ['nullable', 'integer'],
            'supervisor_id' => ['nullable', 'integer', 'exists:users,id'],
            'profile_photo_path' => ['string'],
            'disabled' => ['boolean'],
            'password' => ['nullable', 'string', new Password()]
        ];
    }
}
