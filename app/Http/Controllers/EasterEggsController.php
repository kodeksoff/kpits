<?php

namespace App\Http\Controllers;

use App\Models\SnakeScore;
use Breadcrumbs;
use Illuminate\Http\Request;
use Inertia\Inertia;

class EasterEggsController extends Controller
{
    public function snake()
    {
        Breadcrumbs::set('Пасхалки', route('easter_eggs.snake'));

        return Inertia::render('Games/Snake', [
            'title' => 'ヽ(・∀・)ﾉ Змейка',
            'rating_list' => SnakeScore::orderBy('score', 'desc')->take(50)->get()
        ]);
    }

    public function score(Request $request)
    {
        $user = auth()->user();

        $score = $request->score;

        if (!is_null($user->snakeScore) && $request->score <= $user->snakeScore->score) {
            $score = $user->snakeScore->score;
        }


        return $user->snakeScore()
            ->updateOrCreate(
                ['user_id' => auth()->id()],
                ['score' => $score]
            );
    }
}
