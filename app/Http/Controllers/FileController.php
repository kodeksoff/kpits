<?php

namespace App\Http\Controllers;

use App\Http\Requests\UploadImageRequest;
use Spatie\ImageOptimizer\OptimizerChainFactory;

class FileController extends Controller
{
    public function uploadImage(UploadImageRequest $request)
    {
        $file = $request->file('upload')
            ->storeAs(
                'uploads/' . mb_strtolower($request->path),
                time() . '_' . mb_strtolower($request->file('upload')->getClientOriginalName()),
                'public'
            );

        $optimizerChain = OptimizerChainFactory::create();

        $optimizerChain->optimize(storage_path('app/public') . '/' . $file);

        return response()->json(['url' => '/storage/' .  $file]);
    }
}
