<?php

namespace App\Http\Controllers;

use App\Models\ReportingPeriod;
use App\Models\UserReportingPeriod;
use Illuminate\Support\Facades\Gate;
use Inertia\Inertia;

class MainController extends Controller
{
    public function __invoke()
    {
        return Gate::allows('access-manager')
            ? Inertia::render('Index', [
                'title' => 'Главная',
                'current_period' => ReportingPeriod::periodStatistic(),
                'user_statistic' => UserReportingPeriod::statistic()
            ])
            : to_route('tasks.index');
    }
}
