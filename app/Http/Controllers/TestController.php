<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use FormulaParser\FormulaParser;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class TestController extends Controller
{
    public function __invoke(Request $request)
    {
        $formula_parser = new FormulaParser("100*2-30");

        $data = config('predefinedformulas.managers_conversion.callback')(40, 85);

        return new JsonResponse(data: $data);
    }
}
