<?php

namespace App\Http\Controllers;

use App\Exports\SheetPayment;
use App\Models\ReportingPeriod;
use App\Models\Task;
use App\Models\User;
use App\Models\UserReportingPeriod;
use Breadcrumbs;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Inertia\Inertia;
use Inertia\Response;
use Maatwebsite\Excel\Facades\Excel;
use PhpOffice\PhpSpreadsheet\Writer\Exception;

class ResultController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('viewAny', Task::class);

        abort_if(
            !Gate::allows('access-admin') && !Gate::allows('access-manager') && !Gate::allows('access-accountant'),
            403
        );

        Breadcrumbs::set('Результаты', route('results.index'));

        return Inertia::render('Results/Index', [
            'title' => 'Результаты',
            'user_periods' => UserReportingPeriod::resultsWithFiltering($request),
            'current_user' => $request->user,
            'users' => User::getSubordinatesFromCache(),
            'current_period' => $request->reporting_period ?? ReportingPeriod::currentPeriod()->id,
            'period' => ReportingPeriod::getWithUserPeriodCount($request->reporting_period ?? ReportingPeriod::currentPeriod()->id),
            'periods' => ReportingPeriod::forCurrentYear(),
        ]);
    }

    /**
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws Exception
     */
    public function export(ReportingPeriod $period)
    {
        abort_if(!Gate::allows('access-admin'), 403);

        return Excel::download(new SheetPayment($period->id), 'results.xlsx');
    }
}
