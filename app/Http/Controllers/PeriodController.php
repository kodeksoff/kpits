<?php

namespace App\Http\Controllers;

use App\Contracts\ApprovedPeriodContract;
use App\Contracts\ApprovePeriodContract;
use App\Contracts\PlanningPeriodContract;
use App\Contracts\StorePeriodContract;
use App\Contracts\WorkingPeriodContract;
use App\Enums\PeriodStatus;
use App\Http\Requests\StorePeriodRequest;
use App\Http\Requests\WorkingPeriodRequest;
use App\Models\ReportingPeriod;
use App\Models\UserReportingPeriod;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Gate;

class PeriodController extends Controller
{
    /**
     * @param StorePeriodRequest $request
     * @param StorePeriodContract $storePeriod
     * @return JsonResponse
     */
    public function store(StorePeriodRequest $request, StorePeriodContract $storePeriod)
    {
        abort_if(!Gate::allows('access-admin') && !Gate::allows('access-manager'), 403);

        try {
            $storePeriod($request->validated());
        } catch (Exception $error) {
            return response()->json(['message' => 'Ошибка', 'error' => $error->getMessage()], 500);
        }

        return response()->json(['message' => 'Период создан']);
    }

    /**
     * @param WorkingPeriodRequest $request
     * @param ReportingPeriod $reporting_period
     * @param WorkingPeriodContract $workingPeriod
     * @return JsonResponse
     */
    public function working(
        WorkingPeriodRequest  $request,
        ReportingPeriod       $reporting_period,
        WorkingPeriodContract $workingPeriod
    ) {
        abort_if(!Gate::allows('access-admin') && !Gate::allows('access-manager'), 403);

        try {
            $workingPeriod($request->validated(), $reporting_period);
        } catch (Exception $error) {
            return response()->json(['message' => 'Ошибка']);
        }

        return response()->json(['message' => 'Период отправлен в работу']);
    }

    /**
     * @param UserReportingPeriod $userReportingPeriod
     * @param ApprovePeriodContract $approvePeriod
     * @return JsonResponse
     */
    public function approve(UserReportingPeriod $userReportingPeriod, ApprovePeriodContract $approvePeriod)
    {
        try {
            $approvePeriod($userReportingPeriod);
        } catch (Exception $error) {
            return response()->json(['message' => 'Ошибка', 'error' => $error->getMessage()], 500);
        }

        return response()->json(['message' => 'Период отправлен на проверку']);
    }

    /**
     * @param WorkingPeriodRequest $request
     * @param ReportingPeriod $reporting_period
     * @param PlanningPeriodContract $planningPeriod
     * @return JsonResponse
     */
    public function planning(
        WorkingPeriodRequest   $request,
        ReportingPeriod        $reporting_period,
        PlanningPeriodContract $planningPeriod
    ) {
        abort_if(!Gate::allows('access-admin') && !Gate::allows('access-manager'), 403);

        try {
            $planningPeriod($request->validated(), $reporting_period);
        } catch (Exception $error) {
            return response()->json(['message' => 'Ошибка']);
        }

        return response()->json(['message' => 'Период возарвщен в планирование']);
    }

    /**
     * @param WorkingPeriodRequest $request
     * @param ReportingPeriod $reporting_period
     * @param ApprovedPeriodContract $approvedPeriod
     * @return JsonResponse
     */
    public function approved(
        WorkingPeriodRequest   $request,
        ReportingPeriod        $reporting_period,
        ApprovedPeriodContract $approvedPeriod
    ) {
        abort_if(!Gate::allows('access-admin') && !Gate::allows('access-manager'), 403);

        try {
            $approvedPeriod($request->validated(), $reporting_period);
        } catch (Exception $error) {
            return response()->json(['message' => 'Ошибка']);
        }

        return response()->json(['message' => 'Период утвержден']);
    }

    public function toPayment(ReportingPeriod $reporting_period)
    {
        abort_if(!Gate::allows('access-admin'), 403);

        try {
            $reporting_period->update(['status' => PeriodStatus::in_payment]);
        } catch (Exception $error) {
            return response()->json(['message' => 'Ошибка']);
        }

        return response()->json(['message' => 'Период отправлен на оплату']);
    }
}
