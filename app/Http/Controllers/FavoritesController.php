<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FavoritesController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param Request $request
     * @return void
     */
    public function store(Request $request): void
    {
        auth()->user()->addFavorite($request->user_id);
    }

    public function delete(int $id): void
    {
        auth()->user()->deleteFavorite($id);
    }
}
