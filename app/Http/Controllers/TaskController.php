<?php

namespace App\Http\Controllers;

use App\Contracts\IndexTaskContract;
use App\Contracts\UpdateTaskElementsContract;
use App\Http\Requests\IndexTaskRequest;
use App\Http\Requests\UpdateTaskElementsRequest;
use App\Http\Resources\ReportingPeriodResource;
use App\Models\Task;
use Exception;
use Illuminate\Http\JsonResponse;
use Inertia\Inertia;
use Inertia\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class TaskController extends Controller
{
    /**
     * @param IndexTaskRequest $request
     * @param IndexTaskContract $task
     * @return Response
     */
    public function index(IndexTaskRequest $request, IndexTaskContract $task)
    {
        $task->handle($request);

        return Inertia::render('Task/Index', [
            'title' => 'Мои задачи',
            'current_period' => $task->getCurrentPeriod(),
            'periods' => $task->getReportingPeriods(),
            'period' => Inertia::lazy(fn () => new ReportingPeriodResource($task->getPeriod())),
        ]);
    }

    /**
     * @param UpdateTaskElementsRequest $request
     * @param Task $task
     * @param UpdateTaskElementsContract $taskElements
     * @return JsonResponse
     */
    public function update(UpdateTaskElementsRequest $request, Task $task, UpdateTaskElementsContract $taskElements)
    {
        try {
            $taskElements($task, $request->validated());
        } catch (HttpException|Exception $error) {
            return response()->json([
                'message' => $error->getMessage(),
                'error' => $error->getTraceAsString()
            ], 500);
        }

        return response()->json(['message' => 'Сохранено']);
    }
}
