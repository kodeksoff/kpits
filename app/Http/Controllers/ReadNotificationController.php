<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ReadNotificationController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param Request $request
     * @return void
     */
    public function __invoke(Request $request): void
    {
        auth()->user()->notifications->markAsRead();
    }
}
