<?php

namespace App\Http\Controllers;

use App\Contracts\UpdateTaskContract;
use App\Http\Requests\CreateNewTaskRequest;
use App\Http\Requests\UpdateTaskRequest;
use App\Http\Resources\ReportingPeriodResource;
use App\Models\ReportingPeriod;
use App\Models\Task;
use App\Models\User;
use Breadcrumbs;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Inertia\Inertia;
use Inertia\Response;
use Throwable;

class PlanningController extends Controller
{
    /**
     * @return Response
     */
    public function index(Request $request)
    {
        abort_if(!Gate::allows('access-admin') && !Gate::allows('access-manager'), 403);

        Breadcrumbs::set('Управление задачами', route('planning.list'))->set('Просмотр', route('planning.index'));

        return Inertia::render('Planning/Index', [
            'title' => 'Управление задачами',
            'period' => Inertia::lazy(
                fn () => new ReportingPeriodResource(
                    ReportingPeriod::withUserTasks($request->reporting_period, $request->user, false)->first()
                )
            ),
            'current_user' => $request->user,
            'users' => User::getSubordinatesFromCache(),
            'current_period' => $request->reporting_period ?? ReportingPeriod::currentPeriod()->id,
            'periods' => ReportingPeriod::forCurrentYear(),
            'task_id' => $request->task_id
        ]);
    }


    /**
     * @param Request $request
     * @return Response
     */
    public function list(Request $request)
    {
        abort_if(
            !Gate::allows('access-admin') && !Gate::allows('access-manager') && !Gate::allows('access-accountant'),
            403
        );

        Breadcrumbs::set('Управление задачами', route('planning.list'));

        return Inertia::render('Planning/List/Index', [
            'title' => 'Управление задачами',
            'current_period' => $request->reporting_period ?? ReportingPeriod::currentPeriod()->id,
            'periods' => ReportingPeriod::forCurrentYear(),
            'users' => User::getPeriodWithTaskCount($request)
        ]);
    }

    /**
     * @param CreateNewTaskRequest $request
     * @return JsonResponse
     */
    public function store(CreateNewTaskRequest $request)
    {
        try {
            $taskClass = $request->type;
            $taskClass::createNew($request->validated());
        } catch (Throwable $error) {
            return response()->json(['message' => 'Ошибка', 'error' => $error->getMessage()], 500);
        }

        return response()->json(['message' => 'Сохранено']);
    }


    /**
     * @param UpdateTaskRequest $request
     * @param Task $task
     * @param UpdateTaskContract $updateTask
     * @return JsonResponse
     */
    public function update(UpdateTaskRequest $request, Task $task, UpdateTaskContract $updateTask)
    {
        abort_if(!Gate::allows('access-admin') && !Gate::allows('access-manager'), 403);

        try {
            $updateTask($task, $request->validated());
        } catch (Throwable $error) {
            return response()->json(['message' => 'Ошибка', 'error' => $error->getMessage()], 500);
        }

        return response()->json(['message' => 'Сохранено']);
    }

    /**
     * @param Task $task
     * @return JsonResponse
     */
    public function deleteTask(Task $task)
    {
        abort_if(!Gate::allows('access-admin') && !Gate::allows('access-manager'), 403);

        try {
            $task->delete();
        } catch (Throwable $error) {
            return response()->json(['message' => 'Ошибка', 'error' => $error->getMessage()], 500);
        }

        return response()->json(['message' => 'Удалено']);
    }
}
