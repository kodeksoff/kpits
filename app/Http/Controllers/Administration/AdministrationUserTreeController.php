<?php

namespace App\Http\Controllers\Administration;

use App\Contracts\UpdateUsersTreeContract;
use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateUsersTreeRequest;
use App\Models\User;
use Breadcrumbs;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\JsonResponse;
use Inertia\Inertia;
use Inertia\Response;

class AdministrationUserTreeController extends Controller
{
    /**
     * @return Response
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('viewAny', auth()->user());

        Breadcrumbs::set('Управление', route('administration.index'))
            ->set('Дерево сотрудников', route('administration.users.index'));

        return Inertia::render('Administration/UserTree/Index', [
            'title' => 'Дерево сотрудников',
            'users' => fn () => User::getRecursiveTree()
        ]);
    }

    /**
     * @param UpdateUsersTreeRequest $request
     * @param UpdateUsersTreeContract $userTree
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function change(UpdateUsersTreeRequest $request, UpdateUsersTreeContract $userTree)
    {
        $this->authorize('viewAny', auth()->user());
        try {
            $userTree($request->validated());
        } catch (Exception $error) {
            return response()->json(['message' => $error->getMessage()], 500);
        }

        return response()->json(['message' => 'Дерево обновлено']);
    }
}
