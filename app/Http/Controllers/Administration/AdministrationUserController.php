<?php

namespace App\Http\Controllers\Administration;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Models\Role;
use App\Models\User;
use Breadcrumbs;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Log;
use Inertia\Inertia;
use Inertia\Response;
use Laravel\Fortify\Contracts\CreatesNewUsers;
use Laravel\Fortify\Contracts\UpdatesUserPasswords;
use Laravel\Fortify\Contracts\UpdatesUserProfileInformation;

/**
 *
 */
class AdministrationUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('viewAny', auth()->user());

        Breadcrumbs::set('Управление', route('administration.index'))
            ->set('Список сотрудников', route('administration.users.index'));

        return Inertia::render('Administration/Users/Index', [
            'title' => 'Список сотрудников',
            'users' => User::with(['role'])
                ->paginate(45)
                ->withQueryString()
                ->through(fn ($user) => [
                    'id' => $user->id,
                    'first_name' => $user->first_name,
                    'second_name' => $user->second_name,
                    'phone' => $user->phone,
                    'profile_photo_url' => $user->profile_photo_url,
                    'role' => $user->role->title,
                    'disabled' => $user->disabled,
                    'created_at' => $user->created_at,
                ]),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        Breadcrumbs::set('Управление', route('administration.index'))
            ->set('Список сотрудников', route('administration.users.index'))
            ->set('Новый сотрудник', route('administration.users.create'));

        return Inertia::render('Administration/Users/Create', [
            'title' => 'Регистрация сотрудника',
            'roles' => Role::get(),
            'users' => User::chiefs()->get()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateUserRequest $request
     * @param CreatesNewUsers $newUsers
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function store(CreateUserRequest $request, CreatesNewUsers $newUsers)
    {
        $this->authorize('create', auth()->user());
        try {
            $newUsers->create($request->validated());
        } catch (Exception $error) {
            Log::error('Ошибка создания пользователя', ['error' => $error->getMessage()]);
        }

        return to_route('administration.users.index')->with('flash.banner', 'Пользователь создан');
    }

    /**
     * Display the specified resource.
     *
     * @param User $user
     * @return Response
     * @throws AuthorizationException
     */
    public function show(User $user)
    {
        $this->authorize('viewAny', auth()->user());

        Breadcrumbs::set('Управление', route('administration.index'))
            ->set('Список сотрудников', route('administration.users.index'))
            ->set($user->full_name, route('administration.users.show', $user->id));

        return Inertia::render('Administration/Users/Show', [
            'title' => 'Редактирование сотрудника',
            'user' => $user->load(['role', 'subordinates', 'supervisor']),
            'roles' => Role::get(),
            'users' => User::get()
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param User $user
     * @return Response
     * @throws AuthorizationException
     */
    public function edit(User $user)
    {
        $this->authorize('view', $user);

        Breadcrumbs::set('Управление', route('administration.index'))
            ->set('Список сотрудников', route('administration.users.index'))
            ->set("$user->full_name", route('administration.users.edit', $user->id));

        return Inertia::render('Administration/Users/Edit', [
            'title' => 'Редактирование сотрудника',
            'user' => $user->load(['role', 'subordinates', 'supervisor']),
            'roles' => Role::get(),
            'users' => User::chiefs()->get()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateUserRequest $request
     * @param User $user
     * @param UpdatesUserProfileInformation $updatesUserProfileInformation
     * @param UpdatesUserPasswords $userPassword
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function update(UpdateUserRequest $request, User $user, UpdatesUserProfileInformation $updatesUserProfileInformation)
    {
        $this->authorize('update', $user);

        $updatesUserProfileInformation->update($user, $request->validated());

        return to_route('administration.users.index')->with('flash.banner', 'Пользователь Обновлен');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param User $user
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function destroy(User $user)
    {
        $this->authorize('delete', $user);
        try {
            $user->delete();
        } catch (Exception $error) {
            return response()->json(['message' => 'ошибка'], 500);
        }

        return response()->json(['message' => 'Пользователь удален']);
    }
}
