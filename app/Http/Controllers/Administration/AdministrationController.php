<?php

namespace App\Http\Controllers\Administration;

use App\Http\Controllers\Controller;
use App\Models\Department;
use App\Models\User;
use Breadcrumbs;
use Illuminate\Support\Facades\Gate;
use Inertia\Inertia;
use Inertia\Response;

class AdministrationController extends Controller
{
    /**
     * @return Response
     */
    public function __invoke()
    {
        abort_if(Gate::denies('access-admin'), 403);

        Breadcrumbs::set('Управление', route('administration.index'));

        return Inertia::render('Administration/Index', [
            'users_count' => User::count(),
            'departament_count' => Department::count(),
            'title' => 'Управление',
        ]);
    }
}
