<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class UserPeriodResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'reporting_period_id' => $this->reporting_period_id,
            'status' => $this->status,
            'status_decoding' => $this->status_decoding['description'],
            'payment' => $this->payment,
            'tasks' => TaskResource::collection($this->tasks)
        ];
    }
}
