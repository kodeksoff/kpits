<?php

namespace App\Http\Resources;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ReportingPeriodResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array|Arrayable|\JsonSerializable
     */
    public function toArray($request): array|\JsonSerializable|Arrayable
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'start_period' => $this->start_period,
            'end_period' => $this->end_period,
            'status' => $this->status,
            'status_decoding' => $this->status_decoding['description'],
            'current_date' => $this->current_date,
            'user_period' => new UserPeriodResource($this->userPeriod)
        ];
    }
}
