<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class TaskResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'taskable_type' => $this->taskable_type,
            'taskable_id' => $this->taskable_id,
            'title' => $this->taskable->title,
            'description' => $this->taskable->description,
            'user_id' => $this->user_id,
            'user_reporting_period_id' => $this->user_reporting_period_id,
            'type' => $this->taskable->type,
            'type_decoding' => $this->taskable->type_decoding['description'] ?? null,
            'status' => $this->taskable->status,
            'comment' => $this->comment,
            'elements' => TaskElementResource::collection($this->taskable->elements),
        ];
    }
}
