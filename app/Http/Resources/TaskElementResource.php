<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TaskElementResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'formula' => $this->formula,
            'type' => $this->type ?? null,
            'input' => $this->input ?? null,
            'result' => $this->result,
            'completed' => $this->completed,
            'price' => $this->payment->price ?? 0,
            'order' => $this->order
        ];
    }
}
