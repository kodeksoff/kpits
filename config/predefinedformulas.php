<?php

return [
    'completed_sdek_contracts' => [
        'name' => 'Оформленные договора СДЭК',
        'fields' => [
            [
                'name' => 'Количество договоров',
                'key' => 'count'
            ]
        ],
        'callback' => function (int $count) {
            return (float)$count * 300;
        }
    ],

    'managers_cash_flow' => [
        'name' => 'Менеджеры: денежный оборот',
        'fields' => [
            [
                'name' => 'Оборот',
                'key' => 'turnover'
            ],
            [
                'name' => 'Количество заказов',
                'key' => 'orders'
            ]
        ],
        'callback' => function (int $orders, int $turnover) {
            $ratio = 0;

            if ($orders >= 250 && $orders <= 350) {
                if ($turnover >= 2000000 && $turnover < 2500000) {
                    $ratio = 0.004;
                }

                if ($turnover >= 2500000 && $turnover < 3100000) {
                    $ratio = 0.005;
                }

                if ($turnover >= 3100000 && $turnover < 3900000) {
                    $ratio = 0.006;
                }

                if ($turnover >= 3900000) {
                    $ratio = 0.007;
                }
            }

            if ($orders > 350 && $orders <= 400) {
                if ($turnover >= 2000000 && $turnover < 2500000) {
                    $ratio = 0.005;
                }

                if ($turnover >= 2500000 && $turnover < 3100000) {
                    $ratio = 0.006;
                }

                if ($turnover >= 3100000 && $turnover < 3900000) {
                    $ratio = 0.007;
                }

                if ($turnover >= 3900000) {
                    $ratio = 0.008;
                }
            }

            if ($orders > 400 && $orders <= 450) {
                if ($turnover >= 2000000 && $turnover < 2500000) {
                    $ratio = 0.006;
                }

                if ($turnover >= 2500000 && $turnover < 3100000) {
                    $ratio = 0.007;
                }

                if ($turnover >= 3100000 && $turnover < 3900000) {
                    $ratio = 0.008;
                }

                if ($turnover >= 3900000) {
                    $ratio = 0.009;
                }
            }

            if ($orders > 450) {
                if ($turnover >= 2000000 && $turnover < 2500000) {
                    $ratio = 0.007;
                }

                if ($turnover >= 2500000 && $turnover < 3100000) {
                    $ratio = 0.008;
                }

                if ($turnover >= 3100000 && $turnover < 3900000) {
                    $ratio = 0.009;
                }

                if ($turnover >= 3900000) {
                    $ratio = 0.01;
                }
            }

            return (float)$ratio * $turnover;
        }
    ],

    'managers_conversion' => [
        'Менеджеры: конверсия',
        'fields' => [
            [
                'name' => 'Конверсия оформленных заказов',
                'key' => 'orders'
            ],
            [
                'name' => 'Конверсия выкупленых',
                'key' => 'redeemed'
            ]
        ],
        'callback' => function (int $orders, int $redeemed) {
            $orders_pay = 0;
            $redeemed_pay = 0;

            if ($orders >= 50) {
                $orders_pay = 10000;
            }

            if ($orders >= 45 && $orders < 50) {
                $orders_pay = 7000;
            }

            if ($orders >= 40 && $orders < 45) {
                $orders_pay = 6000;
            }

            if ($orders >= 35 && $orders < 40) {
                $orders_pay = 5000;
            }

            if ($orders >= 30 && $orders < 35) {
                $orders_pay = 4000;
            }

            if ($orders >= 25 && $orders < 30) {
                $orders_pay = 2500;
            }

            if ($redeemed >= 85) {
                $redeemed_pay = 10000;
            }

            if ($redeemed >= 80 && $redeemed < 85) {
                $redeemed_pay = 7000;
            }

            if ($redeemed >= 70 && $redeemed < 80) {
                $redeemed_pay = 6000;
            }

            if ($redeemed >= 65 && $redeemed < 70) {
                $redeemed_pay = 5000;
            }

            if ($redeemed >= 60 && $redeemed < 65) {
                $redeemed_pay = 5000;
            }

            if ($redeemed >= 55 && $redeemed < 60) {
                $redeemed_pay = 4000;
            }

            return $orders_pay + $redeemed_pay;
        }
    ]
];
