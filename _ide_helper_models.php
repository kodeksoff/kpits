<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * App\Models\Department
 *
 * @property int $id
 * @property string $title
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Department newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Department newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Department query()
 * @method static \Illuminate\Database\Eloquent\Builder|Department whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Department whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Department whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Department whereUpdatedAt($value)
 */
	class IdeHelperDepartment {}
}

namespace App\Models{
/**
 * App\Models\ElementPayment
 *
 * @property int $id
 * @property string $elementable_type
 * @property int $elementable_id
 * @property int $user_reporting_period_id
 * @property int $price
 * @property int $counting
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $elementable
 * @property-read \App\Models\UserReportingPeriod $userReportingPeriod
 * @method static \Illuminate\Database\Eloquent\Builder|ElementPayment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ElementPayment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ElementPayment query()
 * @method static \Illuminate\Database\Eloquent\Builder|ElementPayment whereCounting($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ElementPayment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ElementPayment whereElementableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ElementPayment whereElementableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ElementPayment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ElementPayment wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ElementPayment whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ElementPayment whereUserReportingPeriodId($value)
 */
	class IdeHelperElementPayment {}
}

namespace App\Models{
/**
 * App\Models\Favorites
 *
 * @property int $id
 * @property int $user_id
 * @property string $favoriteable_type
 * @property int $favoriteable_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $favoriteable
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|Favorites newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Favorites newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Favorites query()
 * @method static \Illuminate\Database\Eloquent\Builder|Favorites whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Favorites whereFavoriteableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Favorites whereFavoriteableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Favorites whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Favorites whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Favorites whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Favorites withType(string $type)
 */
	class IdeHelperFavorites {}
}

namespace App\Models{
/**
 * App\Models\ReportingPeriod
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $start_period
 * @property string|null $end_period
 * @property \App\Enums\PeriodStatus $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ElementPayment[] $payment
 * @property-read int|null $payment_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Task[] $tasks
 * @property-read int|null $tasks_count
 * @property-read \App\Models\UserReportingPeriod|null $userPeriod
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\UserReportingPeriod[] $userPeriods
 * @property-read int|null $user_periods_count
 * @method static \Illuminate\Database\Eloquent\Builder|ReportingPeriod actualPeriod()
 * @method static \Illuminate\Database\Eloquent\Builder|ReportingPeriod newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ReportingPeriod newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ReportingPeriod query()
 * @method static \Illuminate\Database\Eloquent\Builder|ReportingPeriod whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportingPeriod whereEndPeriod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportingPeriod whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportingPeriod whereStartPeriod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportingPeriod whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportingPeriod whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportingPeriod whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportingPeriod withPayments()
 * @method static \Illuminate\Database\Eloquent\Builder|ReportingPeriod withUserPeriodCount()
 * @method static \Illuminate\Database\Eloquent\Builder|ReportingPeriod withUserTasks(?int $reporting_period = null, ?int $user_id = null, bool $in_work = true)
 */
	class IdeHelperReportingPeriod {}
}

namespace App\Models{
/**
 * App\Models\Role
 *
 * @property int $id
 * @property string $title
 * @property int $is_admin
 * @property \App\Enums\Roles\ResultViewType|null $results_view
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Role newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Role newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Role query()
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereIsAdmin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereResultsView($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereUpdatedAt($value)
 */
	class IdeHelperRole {}
}

namespace App\Models{
/**
 * App\Models\SnakeScore
 *
 * @property int $id
 * @property int $user_id
 * @property int $score
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|SnakeScore newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SnakeScore newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SnakeScore query()
 * @method static \Illuminate\Database\Eloquent\Builder|SnakeScore whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SnakeScore whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SnakeScore whereScore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SnakeScore whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SnakeScore whereUserId($value)
 */
	class IdeHelperSnakeScore {}
}

namespace App\Models{
/**
 * App\Models\Task
 *
 * @property int $id
 * @property string $taskable_type
 * @property int $taskable_id
 * @property int $user_id
 * @property int|null $user_reporting_period_id
 * @property int $order
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \App\Enums\TaskTypes $type
 * @property-read \App\Models\TaskComment|null $comment
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TaskElement[] $elements
 * @property-read int|null $elements_count
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $taskable
 * @property-read \App\Models\User $user
 * @property-read \App\Models\UserReportingPeriod|null $userReportingPeriod
 * @method static \Database\Factories\TaskFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Task newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Task newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Task query()
 * @method static \Illuminate\Database\Eloquent\Builder|Task statusFiltering($request)
 * @method static \Illuminate\Database\Eloquent\Builder|Task whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Task whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Task whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Task whereTaskableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Task whereTaskableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Task whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Task whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Task whereUserReportingPeriodId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Task withPayments()
 */
	class IdeHelperTask {}
}

namespace App\Models{
/**
 * App\Models\TaskComment
 *
 * @property int $id
 * @property int $task_id
 * @property string $comment
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Task $task
 * @method static \Illuminate\Database\Eloquent\Builder|TaskComment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TaskComment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TaskComment query()
 * @method static \Illuminate\Database\Eloquent\Builder|TaskComment whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TaskComment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TaskComment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TaskComment whereTaskId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TaskComment whereUpdatedAt($value)
 */
	class IdeHelperTaskComment {}
}

namespace App\Models{
/**
 * App\Models\TaskElement
 *
 * @property int $id
 * @property int $task_id
 * @property string $title
 * @property int $completed
 * @property int $price
 * @property \App\Enums\TaskElementsType $type
 * @property int $calculated
 * @property int $complete_execution
 * @property int $order
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Task $task
 * @method static \Database\Factories\TaskElementFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|TaskElement newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TaskElement newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TaskElement query()
 * @method static \Illuminate\Database\Eloquent\Builder|TaskElement whereCalculated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TaskElement whereCompleteExecution($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TaskElement whereCompleted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TaskElement whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TaskElement whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TaskElement whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TaskElement wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TaskElement whereTaskId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TaskElement whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TaskElement whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TaskElement whereUpdatedAt($value)
 */
	class IdeHelperTaskElement {}
}

namespace App\Models\TasksElements{
/**
 * App\Models\TasksElements\ElementCheckList
 *
 * @property int $id
 * @property int $task_check_list_id
 * @property string $title
 * @property int $price
 * @property int $completed
 * @property int $order
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\ElementPayment|null $payment
 * @property-read \App\Models\Tasks\TaskCheckList $taskElement
 * @method static \Illuminate\Database\Eloquent\Builder|ElementCheckList newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ElementCheckList newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ElementCheckList query()
 * @method static \Illuminate\Database\Eloquent\Builder|ElementCheckList whereCompleted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ElementCheckList whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ElementCheckList whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ElementCheckList whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ElementCheckList wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ElementCheckList whereTaskCheckListId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ElementCheckList whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ElementCheckList whereUpdatedAt($value)
 */
	class IdeHelperElementCheckList {}
}

namespace App\Models\TasksElements{
/**
 * App\Models\TasksElements\ElementFormula
 *
 * @property int $id
 * @property int $task_formula_id
 * @property string|null $formula
 * @property string $result
 * @property int $price
 * @property int $completed
 * @property int $order
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\ElementPayment|null $payment
 * @property-read \App\Models\Tasks\TaskFormula $taskElement
 * @method static \Illuminate\Database\Eloquent\Builder|ElementFormula newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ElementFormula newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ElementFormula query()
 * @method static \Illuminate\Database\Eloquent\Builder|ElementFormula whereCompleted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ElementFormula whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ElementFormula whereFormula($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ElementFormula whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ElementFormula whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ElementFormula wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ElementFormula whereResult($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ElementFormula whereTaskFormulaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ElementFormula whereUpdatedAt($value)
 */
	class IdeHelperElementFormula {}
}

namespace App\Models\TasksElements{
/**
 * App\Models\TasksElements\ElementSwitch
 *
 * @property int $id
 * @property int $task_switch_id
 * @property string $title
 * @property int $price
 * @property int $completed
 * @property int $order
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\ElementPayment|null $payment
 * @property-read \App\Models\Tasks\TaskSwitch $taskElement
 * @method static \Illuminate\Database\Eloquent\Builder|ElementSwitch newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ElementSwitch newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ElementSwitch query()
 * @method static \Illuminate\Database\Eloquent\Builder|ElementSwitch whereCompleted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ElementSwitch whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ElementSwitch whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ElementSwitch whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ElementSwitch wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ElementSwitch whereTaskSwitchId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ElementSwitch whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ElementSwitch whereUpdatedAt($value)
 */
	class IdeHelperElementSwitch {}
}

namespace App\Models\Tasks{
/**
 * App\Models\Tasks\TaskCheckList
 *
 * @property int $id
 * @property string $title
 * @property string|null $description
 * @property \App\Enums\TaskTypes|null $type
 * @property string|null $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TasksElements\ElementCheckList[] $elements
 * @property-read int|null $elements_count
 * @property-read \App\Models\Task|null $task
 * @method static \Illuminate\Database\Eloquent\Builder|TaskCheckList newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TaskCheckList newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TaskCheckList query()
 * @method static \Illuminate\Database\Eloquent\Builder|TaskCheckList whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TaskCheckList whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TaskCheckList whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TaskCheckList whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TaskCheckList whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TaskCheckList whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TaskCheckList whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TaskCheckList withPayments()
 */
	class IdeHelperTaskCheckList {}
}

namespace App\Models\Tasks{
/**
 * App\Models\Tasks\TaskFormula
 *
 * @property int $id
 * @property string $title
 * @property string|null $description
 * @property \App\Enums\TaskTypes|null $type
 * @property string|null $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TasksElements\ElementFormula[] $elements
 * @property-read int|null $elements_count
 * @property-read \App\Models\Task|null $task
 * @method static \Illuminate\Database\Eloquent\Builder|TaskFormula newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TaskFormula newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TaskFormula query()
 * @method static \Illuminate\Database\Eloquent\Builder|TaskFormula whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TaskFormula whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TaskFormula whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TaskFormula whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TaskFormula whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TaskFormula whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TaskFormula whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TaskFormula withPayments()
 */
	class IdeHelperTaskFormula {}
}

namespace App\Models\Tasks{
/**
 * App\Models\Tasks\TaskPreconfigured
 *
 * @property int $id
 * @property string $title
 * @property string|null $description
 * @property \App\Enums\TaskTypes|null $type
 * @property string|null $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Task|null $task
 * @method static \Illuminate\Database\Eloquent\Builder|TaskPreconfigured newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TaskPreconfigured newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TaskPreconfigured query()
 * @method static \Illuminate\Database\Eloquent\Builder|TaskPreconfigured whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TaskPreconfigured whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TaskPreconfigured whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TaskPreconfigured whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TaskPreconfigured whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TaskPreconfigured whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TaskPreconfigured whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TaskPreconfigured withPayments()
 */
	class IdeHelperTaskPreconfigured {}
}

namespace App\Models\Tasks{
/**
 * App\Models\Tasks\TaskSwitch
 *
 * @property int $id
 * @property string $title
 * @property string|null $description
 * @property \App\Enums\TaskTypes|null $type
 * @property string|null $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TasksElements\ElementSwitch[] $elements
 * @property-read int|null $elements_count
 * @property-read \App\Models\Task|null $task
 * @method static \Illuminate\Database\Eloquent\Builder|TaskSwitch newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TaskSwitch newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TaskSwitch query()
 * @method static \Illuminate\Database\Eloquent\Builder|TaskSwitch whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TaskSwitch whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TaskSwitch whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TaskSwitch whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TaskSwitch whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TaskSwitch whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TaskSwitch whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TaskSwitch withPayments()
 */
	class IdeHelperTaskSwitch {}
}

namespace App\Models{
/**
 * App\Models\User
 *
 * @property int $id
 * @property string $first_name
 * @property string|null $second_name
 * @property string $phone
 * @property string|null $email
 * @property int $role_id
 * @property int|null $counting_id
 * @property int|null $supervisor_id
 * @property string|null $profile_photo_path
 * @property int $disabled
 * @property string $password
 * @property string|null $two_factor_secret
 * @property string|null $two_factor_recovery_codes
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|User[] $children
 * @property-read int|null $children_count
 * @property-read \Illuminate\Database\Eloquent\Collection|User[] $favoriters
 * @property-read int|null $favoriters_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Favorites[] $favorites
 * @property-read int|null $favorites_count
 * @property-read string $profile_photo_url
 * @property-read \App\Models\Favorites|null $hasFavorite
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\UserReportingPeriod[] $periods
 * @property-read int|null $periods_count
 * @property-read \App\Models\Role $role
 * @property-read \App\Models\SnakeScore|null $snakeScore
 * @property-read \Illuminate\Database\Eloquent\Collection|User[] $subordinates
 * @property-read int|null $subordinates_count
 * @property-read User|null $supervisor
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Task[] $tasks
 * @property-read int|null $tasks_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Sanctum\PersonalAccessToken[] $tokens
 * @property-read int|null $tokens_count
 * @method static \Illuminate\Database\Eloquent\Builder|User chiefs()
 * @method static \Database\Factories\UserFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User query()
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCountingId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereDisabled($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereProfilePhotoPath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRoleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereSecondName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereSupervisorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereTwoFactorRecoveryCodes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereTwoFactorSecret($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUpdatedAt($value)
 */
	class IdeHelperUser {}
}

namespace App\Models{
/**
 * App\Models\UserReportingPeriod
 *
 * @property int $id
 * @property int $user_id
 * @property int $reporting_period_id
 * @property \App\Enums\UserPeriodStatus $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TaskElement[] $elements
 * @property-read int|null $elements_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ElementPayment[] $payments
 * @property-read int|null $payments_count
 * @property-read \App\Models\ReportingPeriod|null $reportingPeriod
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Task[] $tasks
 * @property-read int|null $tasks_count
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|UserReportingPeriod inWork()
 * @method static \Illuminate\Database\Eloquent\Builder|UserReportingPeriod newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserReportingPeriod newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserReportingPeriod query()
 * @method static \Illuminate\Database\Eloquent\Builder|UserReportingPeriod whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserReportingPeriod whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserReportingPeriod whereReportingPeriodId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserReportingPeriod whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserReportingPeriod whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserReportingPeriod whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserReportingPeriod withPayments()
 * @method static \Illuminate\Database\Eloquent\Builder|UserReportingPeriod withTasksInWork(int $user_id, bool $in_work)
 */
	class IdeHelperUserReportingPeriod {}
}

