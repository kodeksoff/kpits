<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reporting_periods', function (Blueprint $table) {
            $table->string('status')
                ->index()
                ->default('in_work')
                ->after('end_period');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reporting_periods', function (Blueprint $table) {
            $table->dropColumn('status');
            $table->dropIndex(['status']);
        });
    }
};
