<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('task_elements', function (Blueprint $table) {
            $table->dropIndex(['completed', 'price']);
            $table->dropIndex(['type', 'calculated']);

            $table->index(['calculated', 'complete_execution', 'completed']);
            $table->index(['type', 'price']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
