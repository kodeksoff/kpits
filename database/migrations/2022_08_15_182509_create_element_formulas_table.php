<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('element_formulas', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(\App\Models\Tasks\TaskFormula::class)
                ->constrained()
                ->cascadeOnDelete()
                ->cascadeOnUpdate();
            $table->string('formula')->nullable();
            $table->integer('result')->default(0);
            $table->integer('price')->default(0);
            $table->boolean('completed')->default(0);
            $table->integer('order')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('element_formulas');
    }
};
