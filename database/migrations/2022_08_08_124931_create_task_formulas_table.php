<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('task_formulas', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->text('description')
                ->nullable();
            $table->string('type')
                ->index()
                ->nullable();
            $table->string('status')
                ->index()
                ->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('task_formulas');
    }
};
