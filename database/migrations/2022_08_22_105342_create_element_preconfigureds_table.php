<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('element_preconfigureds', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(\App\Models\Tasks\TaskPreconfigured::class)
                ->constrained()
                ->cascadeOnDelete()
                ->cascadeOnUpdate();
            $table->integer('price')->default(0);
            $table->boolean('completed')->default(0);
            $table->integer('order')->default(0);
            $table->string('type')->nullable();
            $table->json('input')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('element_preconfigureds');
    }
};
