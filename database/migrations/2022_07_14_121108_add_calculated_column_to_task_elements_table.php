<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('task_elements', function (Blueprint $table) {
            $table->boolean('calculated')->default(0)->after('type');
            $table->index(['completed', 'price']);
            $table->index(['type', 'calculated']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('task_elements', function (Blueprint $table) {
            $table->dropColumn('calculated');
        });
    }
};
