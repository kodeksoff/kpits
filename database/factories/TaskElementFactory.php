<?php

namespace Database\Factories;

use App\Models\Task;
use App\Models\TaskElement;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory
 */
class TaskElementFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = TaskElement::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'task_id' => Task::inRandomOrder()->first()->id,
            'title' => $this->faker->text(40),
            'completed' => $this->faker->randomElement([0, 1]),
            'price' => $this->faker->randomNumber(3),
            'type' => $this->faker->randomElement(['checkbox']),
        ];
    }
}
