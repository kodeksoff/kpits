<?php

namespace Database\Factories;

use App\Models\Task;
use App\Models\UserReportingPeriod;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory
 */
class TaskFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Task::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'title' => $this->faker->text(40),
            'description' => $this->faker->text(3500),
            'user_id' => UserReportingPeriod::inRandomOrder()->first()->user_id,
            'user_reporting_period_id' => UserReportingPeriod::inRandomOrder()->first()->id,
            'type' => $this->faker->randomElement(['regular', 'urgent', 'long-term']),
        ];
    }
}
