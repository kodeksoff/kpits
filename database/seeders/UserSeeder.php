<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'first_name' => 'Супервайзер',
                'second_name' => 'Супервайзер',
                'phone' => '+7 (906) 111-11-11',
                'email' => 'mail@gmail.com',
                'role_id' => 1,
                'password' => Hash::make('Sennheiser1337'),
            ],

            [
                'first_name' => 'Андрей',
                'second_name' => 'Оринин',
                'phone' => '+7 (906) 124-13-37',
                'email' => 'kodeksoff@gmail.com',
                'role_id' => 1,
                'password' => Hash::make('Sennheiser1337'),
            ],

            [
                'first_name' => 'Тест',
                'second_name' => 'Тестович',
                'phone' => '+7 (906) 222-22-22',
                'email' => 'test@gmail.com',
                'role_id' => 1,
                'password' => Hash::make('password'),
            ],
        ];

        foreach ($users as $user) {
            User::updateOrCreate(
                ['phone' => $user['phone']],
                $user
            );
        }
    }
}
