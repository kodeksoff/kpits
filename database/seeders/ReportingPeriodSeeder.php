<?php

namespace Database\Seeders;

use App\Models\ReportingPeriod;
use Illuminate\Database\Seeder;

class ReportingPeriodSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $monthName = now()->monthName;
        $monthName = mb_strtoupper(mb_substr($monthName, 0, 1, 'UTF-8'), 'UTF-8').
            mb_substr($monthName, 1, mb_strlen($monthName), 'UTF-8');

        ReportingPeriod::firstOrCreate(
            [
                'title' => $monthName.' '.now()->year,
                'start_period' => now()->startOfMonth(),
                'end_period' => now()->endOfMonth(),
            ],
            [
                'title' => $monthName.' '.now()->year,
                'start_period' => now()->startOfMonth(),
                'end_period' => now()->endOfMonth(),
            ]
        );
    }
}
