<?php

namespace Database\Seeders;

use App\Enums\UserPeriodStatus;
use App\Models\UserReportingPeriod;
use Illuminate\Database\Seeder;

class UserReportingPeriodSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $periods = [
            [
                'user_id' => 1,
                'reporting_period_id' => 1,
                'status' => UserPeriodStatus::planning
            ],
            [
                'user_id' => 2,
                'reporting_period_id' => 1,
                'status' => UserPeriodStatus::planning
            ],
        ];

        foreach ($periods as $period) {
            UserReportingPeriod::updateOrCreate($period, $period);
        }
    }
}
