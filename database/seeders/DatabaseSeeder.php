<?php

namespace Database\Seeders;

use App\Models\Task;
use App\Models\TaskElement;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(): void
    {
        $this->call(RoleSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(ReportingPeriodSeeder::class);
        $this->call(UserReportingPeriodSeeder::class);

        //Task::factory(100)->create();
        //TaskElement::factory(500)->create();
    }
}
