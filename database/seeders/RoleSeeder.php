<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    public function run()
    {
        $roles = [
            [
                'title' => 'Супервайзер',
                'is_admin' => 1,
            ],

            [
                'title' => 'Руководитель',
                'is_admin' => 0,
            ],

            [
                'title' => 'Руководитель отдела',
                'is_admin' => 0,
            ],

            [
                'title' => 'Бухгалтер',
                'is_admin' => 0,
            ],

            [
                'title' => 'Рабочий',
                'is_admin' => 0,
            ],
        ];

        foreach ($roles as $role) {
            Role::updateOrCreate($role, $role);
        }
    }
}
